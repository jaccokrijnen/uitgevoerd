{-# LANGUAGE MultiParamTypeClasses
           , TemplateHaskell
           , TypeFamilies #-}

-- | Defines a general class to unwrap newtype values
module Uitgevoerd.Database.Unwrap 
    ( Unwrap (..)
    , deriveUnwrap)
    where

import Language.Haskell.TH
import Control.Monad

-- | The class of types that are wrappers. Provides a type family
--   that maps the wrapper to its original type, and a function
--   that does the actual unwrapping
class Unwrap a where
    type Original a :: *
    unwrap :: a -> Original a

-- | Template Haskell function that given a list of types (which should be newtypes)
--   generates their instances of Unwrap
deriveUnwrap :: [Name] -> Q [Dec]
deriveUnwrap names =
    do decs <- mapM deriveUnwrapSingle names
       return (concat decs)



-------------------------------------------------------
-- Internal helper functions
-------------------------------------------------------

-- | Derives an instance of the Unwrap class for a given newtype
deriveUnwrapSingle :: Name -> Q [Dec]
deriveUnwrapSingle tyName =
    do info <- reify tyName
       case info of
           TyConI (NewtypeD context _ tyvars con _)
               -> let ty = foldl appT (conT tyName) [varT var | PlainTV var <- tyvars]
                  in  do dec <- instanceD (cxt [])
                                          ((conT ''Unwrap ) `appT` ty)
                                          [ mkAssocType ty (wrappedType con)
                                          , mkUnwrap con]
                         return [dec]
           _   -> fail $ "Could not derive unwrap because " ++ show tyName ++ " is not a newtype."



-- | Constructs the associated type which represents the original (wrapped) type
mkAssocType :: TypeQ -> TypeQ -> DecQ
mkAssocType wrapper original = tySynInstD (''Original) (tySynEqn [wrapper] original)

-- | Constructs the unwrap function for the constructor of the newtype
mkUnwrap :: Con -> DecQ 
mkUnwrap (NormalC cName types) = 
    let name         = 'unwrap
        unwrapClause = do x <- newName "x"
                          clause [conP cName [varP x]] (normalB $ varE x) []
    in  funD name [unwrapClause]
mkUnwrap _ = failRecord


-- Gets the type of the (only) field of the (only) constructor
wrappedType :: Con -> TypeQ
wrappedType (NormalC _ [(_, t)]) = return t
wrappedType _ = failRecord


failRecord = fail "Jacco should add support for constructors with record syntax ..."