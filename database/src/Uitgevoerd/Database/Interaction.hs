{-# LANGUAGE DeriveDataTypeable,
             GeneralizedNewtypeDeriving,
             RecordWildCards,
             TemplateHaskell,
             TypeFamilies,
             OverloadedStrings,
             FlexibleContexts #-}

-- | Defines the ACID actions for interacting with the database
module Uitgevoerd.Database.Interaction (InsertEvent (..),
                                        RemoveEvent (..),
                                        QNextEventId (..),
                                        OverwriteEventAt (..),
                                        QTitle (..),
                                        QQuery (..),
                                        QSize  (..),
                                        QUrl   (..),
                                        QAll   (..),
                                        QVenue (..)
                                        )

  where


import Control.Monad.State
import Control.Monad.Reader
import Control.Applicative

import qualified Data.IxSet as Ix (insert, deleteIx)
import Data.IxSet hiding (insert, deleteIx, null)
import Data.Typeable hiding (Proxy)
import Data.Acid hiding (query)
import qualified Data.Text as T
import Uitgevoerd.Database.Types


-- * General operations


-- | Inserts an event into the database
insertEvent :: Event -> Update Database ()
insertEvent event = do db@Database{..} <- get
                       let Table{..} = events
                       put $ db { events = events { nextId  = succ nextId,
                                                    ixset = Ix.insert event ixset } }

-- | Removes an Event from the database
removeEvent :: EventId -> Update Database ()
removeEvent index =
    do db@Database{..} <- get
       let Table{..} = events
       put $ db { events = events {ixset = Ix.deleteIx index ixset } }

-- | Overwrites an event with the given eventId
overwriteEventAt :: EventId -> Event -> Update Database ()
overwriteEventAt key event =
    do db@Database {..} <- get
       let Table{..} = events
       put db {events = events {ixset = updateIx key event ixset} }

-- | Returns the next id, does not update it
qNextEventId :: Query Database EventId
qNextEventId =
    do db@Database{..} <- ask
       let Table{..} = events
       return (EventId nextId)



-- * Specific queries on the data

-- | Search the title on given keywords
qTitle :: [T.Text] -> Query Database [Event]
qTitle strs = do db@Database{..} <- ask
                 let Table{..} = events
                 return . toAscList (Proxy :: Proxy Date) $ ixset @+ strs          -- match on any of the strings

-- | Find all events at the given venue
qVenue :: T.Text -> Query Database [Event]
qVenue t = do
    db@Database {..} <- ask
    let Table{..} = events
    return . toAscList (Proxy :: Proxy Date) $ ixset @* [Venue t]

-- | Complete query
qQuery :: [T.Text]                -- ^ The keywords
       -> [T.Text]                -- ^ The location keywords
       -> Query Database [Event]
qQuery keywords locationWords =
    do db@Database{..} <- ask
       let Table{..}   = events

       return . toAscList (Proxy :: Proxy Date) $ ixset @+? map LocationWord locationWords
                                                        @+? keywords

-- | Creates the subset that has an index in the provided list, except for
--   the empty list on which it acts as the identity
(@+?) :: (Indexable a, Typeable a, Ord a, Typeable k) => IxSet a -> [k] -> IxSet a
ixset @+? [] = ixset
ixset @+? xs = ixset @+ xs

-- | Get the amount of events in the database
qSize :: Query Database Int
qSize = size . ixset . events <$> ask

qUrl :: Url -> Query Database [Event]
qUrl url =
  do Database {..} <- ask
     let Table{..} = events
     return  . toList $ ixset @= url

qAll :: Query Database [Event]
qAll =
    do Database {..} <- ask
       let Table{..} = events
       return  . toList $ ixset

$(makeAcidic ''Database
    [ 'insertEvent,
      'removeEvent,
      'overwriteEventAt,
      'qNextEventId,
      'qTitle,
      'qSize,
      'qUrl,
      'qQuery,
      'qVenue,
      -- Testing
      'qAll
    ])
