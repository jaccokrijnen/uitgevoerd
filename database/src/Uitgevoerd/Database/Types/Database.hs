{-# LANGUAGE TemplateHaskell
		   , DeriveDataTypeable #-}
-- | This module defines the datatype for the complete database
module Uitgevoerd.Database.Types.Database where

import Data.Typeable hiding (Proxy)
import Data.SafeCopy
import Data.Data hiding (Proxy)
import Data.Default
import Data.IxSet as IxSet

import Uitgevoerd.Database.Types.Event
import Control.Applicative


-- | The database consists of two tables, events and locations
data Database = Database {
            events   :: Table Event
          }
    deriving (Data, Typeable)

-- | A Table consists of an ixset together with the next unused id
data Table a = Table {
    nextId :: Integer,
    ixset  :: (IxSet a)
}
    deriving (Data, Typeable)



-- * Instances for default values

instance Indexable a => Default (Table a) where
    def = Table 0 IxSet.empty

instance Default Database where
    def = Database def



-- * Instances for serialization (SafeCopy)

-- The Table instance is by hand, because it is polymorphic
instance (SafeCopy a,                     -- constraint for using safeGet, safePut
		  Indexable a, Typeable a, Ord a) -- constraints for IxSet to be instance of SafeCopy
         => SafeCopy (Table a) where
	version = 0
	getCopy 				     = contain $ Table <$> safeGet <*> safeGet
	putCopy table = contain $ safePut (nextId table) >> safePut (ixset table)

$(deriveSafeCopy 0 'base ''Database)