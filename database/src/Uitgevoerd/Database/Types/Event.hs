{-# LANGUAGE DeriveDataTypeable
           , GeneralizedNewtypeDeriving
           , RecordWildCards
           , TemplateHaskell
           , TypeFamilies
           , OverloadedStrings
           , FlexibleContexts
           , MultiParamTypeClasses
           , FlexibleInstances #-}

-- | Defines the datatypes for the entries in the database
module Uitgevoerd.Database.Types.Event  where

import Data.Char
import Data.Typeable hiding (Proxy)
import Data.SafeCopy
import Data.Data hiding (Proxy)
import Data.IxSet
import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.ByteString.Lazy.Char8 as B

import Uitgevoerd.Database.Unwrap

-- | An entry is a record that corresponds to the database scheme.
-- Use @EntryId (-1)@ for posts that are not yet stored in the database
data Event = Event {
    entryId     :: EventId,
    title       :: Title,
    location    :: Location,
    dateTimes   :: DateTimes,

    performers  :: Performers,
    program     :: Program,
    genre       :: Genre,
    url         :: Url,
    price       :: Price,
    image       :: Image,
    description :: Description
} deriving (Show, Eq, Ord, Data, Typeable)

----------------------------------------------------------
-- Attributes
----------------------------------------------------------

-- * Mandatory
newtype EventId     = EventId    Integer                     deriving (Show, Eq, Ord, Enum, Data, Typeable)
newtype Url         = Url        Text                      deriving (Show, Eq, Ord, Data, Typeable)
newtype Title       = Title      Text                      deriving (Show, Eq, Ord, Data, Typeable)
newtype DateTimes   = DateTimes  [(Date, Times)]             deriving (Show, Eq, Ord, Data, Typeable)


data Location = Location {
    _province   :: Province,
    _town       :: Town,
    _venue      :: Venue
} deriving (Eq, Ord, Show, Data, Typeable)

newtype Province   = Province Text  deriving (Show, Eq, Ord, Data, Typeable)
newtype Town       = Town Text      deriving (Show, Eq, Ord, Data, Typeable)
newtype Venue      = Venue Text     deriving (Show, Eq, Ord, Data, Typeable)


-- * Optional
-- (Euros, cents)
newtype Price       = Price       (Maybe (Int, Int))     deriving (Show, Eq, Ord, Data, Typeable)
-- ([(Composer, Piece)])
newtype Program     = Program     (Maybe [(Text, Text)]) deriving (Show, Eq, Ord, Data, Typeable)
newtype Performers  = Performers  (Maybe [Text])         deriving (Show, Eq, Ord, Data, Typeable)
newtype Genre       = Genre       (Maybe Text)           deriving (Show, Eq, Ord, Data, Typeable)
newtype Image       = Image       (Maybe FilePath)       deriving (Show, Eq, Ord, Data, Typeable)
newtype Description = Description (Maybe Text)           deriving (Show, Eq, Ord, Data, Typeable)


type Date  = (Int, Int, Int)    -- year, month, day
type Times = [(Int, Int)]       -- hours, minutes



-- * Instances for SafeCopy
$(deriveSafeCopy 0 'base ''Event)
$(deriveSafeCopy 0 'base ''EventId)
$(deriveSafeCopy 0 'base ''Url)
$(deriveSafeCopy 0 'base ''Title)
$(deriveSafeCopy 0 'base ''DateTimes)
$(deriveSafeCopy 0 'base ''Price)
$(deriveSafeCopy 0 'base ''Program)
$(deriveSafeCopy 0 'base ''Performers)
$(deriveSafeCopy 0 'base ''Genre)
$(deriveSafeCopy 0 'base ''Image)
$(deriveSafeCopy 0 'base ''Description)
$(deriveSafeCopy 0 'base ''Location)
$(deriveSafeCopy 0 'base ''Venue)
$(deriveSafeCopy 0 'base ''Province)
$(deriveSafeCopy 0 'base ''Town)

-- * Instances for Unwrap
$(deriveUnwrap [''EventId, ''Title, ''DateTimes,
                ''Price, ''Performers, ''Genre, ''Image, ''Description, ''Url, ''Province, ''Venue, ''Town])

-----------------------------------------------------------
-- Indexing
-----------------------------------------------------------


instance Indexable Event where
    -- | The indexing functions for the IxSet
    empty = ixSet [ ixGen (Proxy :: Proxy EventId)
                  , ixGen (Proxy :: Proxy Url)
                  , ixFun venueName
                  , ixFun firstDateIx
                  , ixFun getKeywords
                  , ixFun locationWords]

instance Indexable Location where
  -- | The indexing functions for the IxSet
  empty = ixSet [ ixGen (Proxy :: Proxy Province)
                  , ixGen (Proxy :: Proxy Town)
                  , ixGen (Proxy :: Proxy Venue)]

venueName :: Event -> [Venue]
venueName Event {..} = [_venue location]

firstDateIx :: Event -> [Date]
firstDateIx e = case dateTimes e of
                    (DateTimes ((d,_):_)) -> [d]
                    (DateTimes [])        -> []

getKeywords :: Event -> [T.Text]
getKeywords e = titleWords e ++ descrWords e

-- This is a quick workaround because indices are based on the types,
-- so having [Text] would clash  with the getKeywords function
newtype LocationWord = LocationWord T.Text deriving (Data, Typeable, Show, Eq, Ord)
locationWords :: Event -> [LocationWord]
locationWords e = map (LocationWord . T.map toLower) [unwrap (_venue loc), unwrap (_town loc)]
    where loc = location e

titleWords :: Event -> [T.Text]
titleWords event = let (Title t) = title event
                   in  T.words . T.map toLower $ t

descrWords :: Event -> [T.Text]
descrWords event = let (Description mbDescr) = description event
                   in  case mbDescr of
                           Just d -> T.words . T.map toLower $ d
                           Nothing -> []
