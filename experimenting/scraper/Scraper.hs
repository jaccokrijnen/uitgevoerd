module Scraper where

import Data.Maybe
import Data.Char

import Network.HTTP
import Network.Browser as B
import Network.URI
import Text.HTML.TagSoup
import qualified Data.ByteString.Lazy as L

import Control.Monad

data Info = Info {name :: String, start :: String, location :: String}
    deriving Show



openUrl :: String -> IO String
openUrl x = 
    do 
      (_, rsp) <- B.browse $ do
                    setAllowRedirects True -- handle HTTP redirects
                    request $ Request (fromJust $ parseURI x) GET
                                [Header HdrCookie "__ubic1=MTE3ODM0NDM0MTRjN2RkYTA1OTAzMmU4LjkxODE1Njk2; __utma=46363135.421215970.1283316265.1283538085.1283541700.10; __utmz=46363135.1283316265.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none); __utmc=46363135; PHPSESSID=59a130c66d4853f85289852f15cefa3a; resolution=1920x1080; ip_auto_login[login]=cap11235; ip_auto_login[password_md5]=NDM0NWM0NDlkZTg4MjRkMWVhZmJmZWNiZTQwOWQ4YTE%3D; __utmb=46363135"] ""
      return (rspBody rsp)

-- | Downloads the page to "grabbed.htm", for analyzing a html page
grab str = do src <- openUrl str
              writeFile "grabbed.htm" src



main =
    do urls <- scrapePage 0 -- readFile "agenda.htm"
       infos <- forM urls $ \url ->
                    do concertTags <- fmap parseTags $ openUrl url-- readFile "concert.htm"
                       return $ scrapeInfo concertTags
       mapM_ print $ infos -- scrapeUrls tags


scrapePage :: Int -> IO [String]
scrapePage n = 
    do tags          <- fmap parseTags $ openUrl (agendaPage n)
       let (urls, cont)  = scrapeUrls tags
       rest          <- if cont 
                           then scrapePage (n+1) 
                           else return []
       return $ urls ++ rest

agendaPage x = "http://m.vredenburg.nl/nl/concerten/?p=" ++ show x








----------------------
-- Op agendapagina
----------------------

scrapeUrls :: [Tag String] -> ([String], Bool)
scrapeUrls tags = 

       let -- Is there a next page to continue?
           cont  = (TagText "volgende") `elem` tags 
           
           -- Relevant urls on page
           urls  = do event <- sections ( ~== "<li class=event>" ) $ tags
                      let (a:_) = dropWhile (~/= "<a>") event
                      
                      -- Construct concert url
                      let url   = "http://m.vredenburg.nl" ++ fromAttrib "href" a
                      return url
                     
        in (urls, cont)



-----------------------
-- Op concertpagina
-----------------------



-- | Een pagina met info over vredenburg concert
testPage = "http://www.vredenburg.nl/agenda/concerten/3015/Instrumentaal_Ensemble_Radio_Filharmonisch_Orkest"          



scrapeInfo tags = let (TagText name)  : _ = dropWhile (~/= TagText "") . drop 1 . dropWhile (~/= "<span class=title>") $ tags
                      (TagText start) : _ = dropWhile (~/= TagText "") . drop 1 . dropWhile (~/= TagText "Datum:") $ tags
                      (TagText loc)   : _ = dropWhile (~/= TagText "") . drop 1 . dropWhile (~/= TagText "Aanvang:") $ tags
                  in Info name start loc

