/**
 * Common functions/objects
 * 
 * @author AK
 */

function isEmpty(str) { if (str === null || str === '' || str == 0) { return true; } return false; }


// twitter gebruikt het //
function isEmpty(str) { if (typeof str == 'undefined' || str === null || str === '' || str == 0) { return true; } return false; }


/**
 * peppered config
 */
$.ppprd = {
	debug: {
		enabled: true,
		logAlerts: false	// alerts for clients that don't have the firebug console (otherwise silent)
	}
};


/**
 * debug/logging
 */
jQuery.log = function(message) {
	if (!$.ppprd.debug.enabled) {
		return false;
	}
	if (window.console && window.console.debug) {
		console.debug(message);
	} else {
		if ($.ppprd.debug.logAlerts) {
			alert(message);
		}
		else {
			return;
		}
	}
};


$(function(){
     $('body').addClass('js');
	
	// anchors (external)
	$("a[rel='external']").click(function(){window.open(this.href); return false});
	
	/**
	 * Auto-submit News & Concert overview month filter
	 */
	$('input#toonNieuws').hide();
	$('input#toonConcerten').hide();
	
	$('#formVoorstellingen select').change(function() {
		$('#formVoorstellingen').submit();
	});
	
	$('#formNieuws select').change(function() {
		$('#formNieuws').submit();
	}); 
	
	// refresh queue (prevent timeout)
	if (typeof Xajax != 'undefined') 
		window.setInterval("xajax_UpdateActivity()", 270000);
});


/**
 * bg img resize function
 * @author AK
 */
$(function(){
	var imgContainer = $('#bgImgWrap');
	var img = imgContainer.children('img');
	var contentDiv = $('#columnContainer');
	
	var contentWidth = contentDiv.width();
	var contentHeight = contentDiv.height();

	var imgXAspect = bgImgWidth/bgImgHeight;
	var imgYAspect = bgImgHeight/bgImgWidth;
	
	var minWidth = 800;	/* min.width the bg image should have */
		
	function resize() {
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();
		
		img.width('100%');
		var newImgWidth  = img.width();
		
		// not shorter than minWidth
		if (newImgWidth < minWidth) {
			newImgWidth = minWidth;
		}
		var newImgHeight = newImgWidth * imgYAspect;
		
		// height not shorter than viewportHeight or contentHeight
		if (newImgHeight < viewportHeight) {
			imgContainer.css('overflowY', 'visible');
			if (newImgHeight < viewportHeight) {
				imgContainer.height(viewportHeight);
				newImgHeight = viewportHeight;
			}
			else {				
				imgContainer.height(contentHeight);
				newImgHeight = contentHeight;
			}
			newImgWidth = newImgHeight * imgXAspect;
		}
	
		// set proper imgContainer height & hide Y-overflow (no unnecessary y-space/v.scrollbar)
		if (newImgHeight > contentHeight && contentHeight > viewportHeight) {
			imgContainer.height(contentHeight);
			imgContainer.css('overflowY', 'hidden');
		}
		else if (contentHeight < viewportHeight) {
			imgContainer.height(viewportHeight);
			imgContainer.css('overflowY', 'hidden');
		}
		else {
			imgContainer.height('100%');
			imgContainer.css('overflowY', 'visible');
		}	
		
		// set width & hide X-overflow for imgContainer if larger than contentWidth (no unnecessary x-space/h.scrollbar)
		if (newImgWidth > viewportWidth) {
			if (contentWidth > viewportWidth) {
				imgContainer.width(contentWidth);
			}
			else {
				imgContainer.width('100%');
			}
			imgContainer.css('overflowX', 'hidden');
		}
		else {
			imgContainer.width('100%');
			imgContainer.css('overflowX', 'visible');
		}
		
		// set the new width & height
		newImgWidth = Math.round(newImgWidth);
		newImgHeight = Math.round(newImgHeight);
		
		img.width(newImgWidth);
		img.height(newImgHeight);
		
		// set cookie so inital w&h can be set via css for smooth browsing experience
		document.cookie = 'bgImgWidth='+newImgWidth+'; path=/';
		document.cookie = 'bgImgHeight='+newImgHeight+'; path=/';
	}
	
	$(window).bind('resize', function(){
		resize();	
	});
	
	resize();
});
