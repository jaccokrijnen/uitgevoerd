/**
 * sIFR config/execution
 * 
 * prereq: sIFR 3(.436), jQuery (1.3.2)
 * load: body
 * 
 * Customized with jQuery code
 * Main reason for a lot of the customization was because of the web &
 * replacement font really differed in vertical geometry
 * With some trickery we minimize/prevent vertical shifting
 * 
 * beta version
 * 
 * @author AK
 */


if (typeof sIFR !== 'undefined') {
	var sIFRFont = {
		src: '/flash/sifr-vredenburg.swf',
		ratios: [6, 1.24, 11, 1.21, 19, 1.13, 27, 1.09, 31, 1.07, 32, 1.08, 40, 1.07, 62, 1.06, 63, 1.05, 64, 1.06, 109, 1.05, 114, 1.04, 122, 1.05, 1.04]
	};
	
	sIFR.useStyleCheck = true;
	//sIFR.fitExactly = true;
	//sIFR.useDomLoaded = false; // default: true
	//sIFR.fixWrap = true;
	//sIFR.forceClear = true;
	//sIFR.forceWidth = false;
	//sIFR.forceTextTransform = true;
	//sIFR.preserveSingleWhitespace  = false;
	
	var iHeightCorrectionExtra = 0;
	if ($.browser.msie) {
		iHeightCorrectionExtra = 4;
	}
	
	var oElements = {
		0: {
			selector: '#content #nieuwsItem h3',
			heightCorrection: 7 + iHeightCorrectionExtra,
			leading: -6,
			preventWrap: true,
			color: '#df2626'
		},
		1: {
			selector: '.contentItem h3.redDash',
			heightCorrection: 19,
			leading: -8,
			color: '#df2626'
		},
		2: {
			selector: '.contentItem h3.contentText',
			heightCorrection: 19,
			leading: -8,
			color: '#df2626'
		},
		3: {
			selector: '#bodyConcerten #content .mainInfo h2',
			heightCorrection: 3,
			leading: -6,
			color: '#ffffff'
		},
		4: {
			selector: '#bodyConcerten #content.showOverview h2',
			heightCorrection: 3,
			leading: -6,
			color: '#ffffff'
		},
		5: {
			selector: '#orderSerie h2',
			heightCorrection: 3,
			leading: -6,
			color: '#ffffff'
		},	
		6: {
			selector: '#menuBanners li a',
			heightCorrection: -50,
			leading: 0,							// must be 0 - due to the replaceText() trick onRollOver
			color: '#000000',
			a_link: 'color: #000000;',
			a_hover: 'color: #df2626;',
			offsetLeft: 8,
			offsetTop: 6,
			fontSize: '24px',
			tuneHeight: 5,
			sharpness: -120,
			onRollOver: function() {
				// to mimic block-level links (as menu). ReplaceText trickery needed, otherwise Flash movie won't be updated (FF 6)
				var text = $('#' + this.id + '_alternate').text();
				this.changeCSS('.sIFR-root { color: #DF2626; }');
				this.replaceText(text.toUpperCase() + " ");
			},
			onRollOut: function() {
				var text = $('#' + this.id + '_alternate').text();
				this.changeCSS('.sIFR-root { color: #000000; }');
				this.replaceText(text.toUpperCase().substr(0,text.length-1));
			},
			onRelease: function() {
				var $a = $(this.getFlashElement()).parent();
				window.location.href = $a.attr('href');
			}
		}
	};
	
	var oReplacements = {};
	
	$.each(oElements, function(i) {
		var $this = $(this.selector),
			iHeightCorrection = this.heightCorrection,
			iLeading = this.leading,
			sColor = this.color,
			sA_link = this.a_link || 'color: #df2626;',
			sA_hover= this.a_hover || 'color: #000000;',
			offsetLeft = this.offsetLeft || 0,
			offsetTop = this.offsetTop || 0,
			fontSize = this.fontSize || null,
			tuneHeight = this.tuneHeight || 0,
			sharpness = this.sharpness || 0,
			onRollOver = this.onRollOver || null,
			onRollOut = this.onRollOut || null,
			onRelease = this.onRelease || null
		; 
		
		var bPreventWrap = (typeof this.preventWrap !== 'undefined' && this.preventWrap === true) ? true : false;
		if ($this.length) {
			$this.each(function(j) {
				var $this = $(this);
				var sClassName = 'sIFR-toReplace-' + i + '-' + j;
				$this.addClass('sIFR-toReplace ' + sClassName);
				var iHeight = $this.height() + iHeightCorrection;
				oReplacements[i+j] = {
					selector: '.' + sClassName,
					height: iHeight,
					leading: iLeading,
					preventWrap: bPreventWrap,
					color: sColor,
					a_link: sA_link,
					a_hover: sA_hover,
					offsetLeft: offsetLeft,
					offsetTop: offsetTop,
					fontSize: fontSize,
					tuneHeight: tuneHeight,
					sharpness: sharpness,
					onRollOver: onRollOver,
					onRollOut: onRollOut,
					onRelease: onRelease
				};
			});
		}
	});
	
	sIFR.activate(sIFRFont);
	
	// in IE8 sIFR doesn't wrap properly at times
	var iMarginRight = 0;
	if ($.browser.msie && parseFloat($.browser.version) == 8) {
		iMarginRight = 6;
	}
	
	$.each(oReplacements, function() {
		var oReplacement = this;
		$(oReplacement.selector).height(oReplacement.height);
		sIFR.replace(sIFRFont, {
			selector: oReplacement.selector,
			css: [
				'.sIFR-root { ' + (oReplacement.fontSize ? 'font-size: '+oReplacement.fontSize+';' : '') + 'margin-right: ' + iMarginRight + 'px; color: ' + oReplacement.color + '; text-transform: uppercase; leading: ' + oReplacement.leading + ';  }',
				'a { text-decoration: none; }',
				'a:link { ' + oReplacement.a_link + ' }',
				'a:hover { ' + oReplacement.a_hover + ' }',
				'.sIFR-root:hover { ' + oReplacement.a_hover + ' }'
			],
			onReplacement: function() {
				$(oReplacement.selector).css('height', 'auto');
			},
			forceSingleLine : oReplacement.preventWrap,
			wmode: 'transparent',
			offsetLeft: oReplacement.offsetLeft,
			offsetTop: oReplacement.offsetTop,
			tuneHeight: oReplacement.tuneHeight,
			antiAliasType: 'advanced',
			sharpness: oReplacement.sharpness,
			onRollOver: oReplacement.onRollOver,
			onRollOut: oReplacement.onRollOut,
			onRelease: oReplacement.onRelease
		});
	});

}
