{-# LANGUAGE CPP, DeriveDataTypeable, FlexibleContexts, GeneralizedNewtypeDeriving, 
    MultiParamTypeClasses, TemplateHaskell, TypeFamilies, RecordWildCards #-}
module Main (module Main)  where

import Control.Monad
import Control.Exception
import Control.Applicative
import Control.Monad.Reader
import Control.Monad.State
import Data.Data

import Happstack.Server

import Data.Acid
import Data.Acid.Advanced
import Data.Acid.Local
import Data.SafeCopy

---------
-- ACID
---------

-- 1. Define datatype
data CounterState = CounterState {count :: Integer}
    deriving (Eq, Ord, Read, Show, Data, Typeable)

-- 2. Derive serialization/deserialization (instance of SafeCopy) 
$(deriveSafeCopy 0 'base ''CounterState)


-- 3. Write some functions on the data (Update ~= State, Query ~= Reader)
initCounter = CounterState 0

incCounter :: Integer -> Update CounterState Integer
incCounter n = 
    do c@CounterState{..} <- get
       let count' = count + n
       put $ c {count = count'}
       return $ count'

peekCounter :: Query CounterState Integer
peekCounter = count <$> ask

-- 4. Derive AcidState events
$(makeAcidic ''CounterState ['incCounter, 'peekCounter])









main :: IO ()
main = do bracket (openLocalState initCounter)
                  (createCheckpointAndClose)
                  (\acid -> simpleHTTP nullConf $ handlers acid)


handlers :: AcidState CounterState -> ServerPart String
handlers acid = 
    msum [ do q <- look "q"
              c <- update' acid (IncCounter 1)
              ok $ "Search #" ++ show c]




-----------
-- IxSet
-----------

