{-# LANGUAGE DeriveDataTypeable, GeneralizedNewtypeDeriving, RecordWildCards,
             TemplateHaskell, TypeFamilies, OverloadedStrings, FlexibleContexts #-}
module DBTest where

import Control.Applicative hiding (empty)

import Data.Typeable
import Data.SafeCopy
import Data.Data
import Data.IxSet

import qualified Text.ParserCombinators.UU.Core as UU
import Text.ParserCombinators.UU hiding (parse, empty)
import qualified Text.ParserCombinators.UU as UU
import Text.ParserCombinators.UU.Utils
import Text.ParserCombinators.UU.BasicInstances hiding (Parser,input,msgs)
type Parser a = P (Str Char String LineCol) a





data Concert = Concert {
    concertId   :: ConcertId,
    name        :: Name,
    city        :: City,
    price       :: Price,
    description :: Description
} deriving (Show, Eq, Ord, Data, Typeable)

newtype ConcertId   = ConcertId Int    deriving (Show, Eq, Ord, Data, Typeable, SafeCopy)
newtype Name        = Name String      deriving (Show, Eq, Ord, Data, Typeable, SafeCopy)
newtype City        = City String      deriving (Show, Eq, Ord, Data, Typeable, SafeCopy)
newtype Price       = Price (Int, Int) deriving (Show, Eq, Ord, Data, Typeable, SafeCopy)
newtype Description = Description String deriving (Show, Eq, Ord, Data, Typeable, SafeCopy)


instance Indexable Concert where
    empty = ixSet [ixGen (Proxy :: Proxy ConcertId)
                  ,ixGen (Proxy :: Proxy City)
                  ,ixFun ((\(Description str) -> words str) . description)
                  ]


parse :: Parser a -> String -> a
parse p input = case UU.parse ( (,) <$> p <*> UU.pEnd) (createStr (LineCol 1 1) input) of
                  (a,[]  )  -> a
                  (a, errs) -> a

db :: IO (IxSet Concert)
db = readFile "testdata" >>= return . foldr insert empty . map (parse parseConcert) . lines 





parseConcert :: Parser Concert
parseConcert = Concert <$> pConcertId <*> pName <*> pCity <*> pPrice <*> pDescription
    where pConcertId   = ConcertId <$> pNatural
          pName        = Name      <$> pStr
          pCity        = City      <$> pStr
          pPrice       = Price     <$> ((,) <$> pNatural <* pSym ',' <*> pNatural)
          pDescription = Description <$> pStr

          pStr       = pPacked (pSym '"') (pSym '"') (pMunch (/= '"'))