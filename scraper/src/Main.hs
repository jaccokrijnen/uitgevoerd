{-# LANGUAGE OverloadedStrings #-}
module Main (module Main) where

import Control.Monad
import Control.Applicative
import Control.Exception (bracket, catch, SomeException)

import Data.Maybe
import Data.Default
import Data.Acid
import Data.Acid.Local (createCheckpointAndClose)
import Data.Acid.Advanced hiding (Event)
import qualified Data.Text as T
import qualified Data.Text.IO as T

import Text.HTML.TagSoup
import Text.JSON
import Text.Printf

import System.FilePath
import System.Directory
import System.Environment

import Uitgevoerd.Scraper.Utils
import Uitgevoerd.Scraper

import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction
import Uitgevoerd.Database.Unwrap
import Paths
import Data.Time.LocalTime

import qualified Uitgevoerd.Scrapers.Concertgebouw as Concertgebouw
import qualified Uitgevoerd.Scrapers.DeDoelen   as DeDoelen
import qualified Uitgevoerd.Scrapers.DeVereeniging as DeVereeniging
import qualified Uitgevoerd.Scrapers.FFMZ as FFMZ
import qualified Uitgevoerd.Scrapers.MGIJ as MGIJ
import qualified Uitgevoerd.Scrapers.MusisSacrum as MusisSacrum
import qualified Uitgevoerd.Scrapers.ParktheaterEindhoven as ParktheaterEindhoven
import qualified Uitgevoerd.Scrapers.RotterdamseSchouwburg as RotterdamseSchouwburg
import qualified Uitgevoerd.Scrapers.SSBA as SSBA
import qualified Uitgevoerd.Scrapers.SSBG as SSBG
import qualified Uitgevoerd.Scrapers.SSBU as SSBU
import qualified Uitgevoerd.Scrapers.TivoliVredenburg as TivoliVredenburg


main :: IO ()
main = return ()


allScrapers :: [Scraper]
allScrapers = [ Concertgebouw.scraper
              , DeDoelen.scraper
              , DeVereeniging.scraper
              , FFMZ.scraper
              , MGIJ.scraper
              , MusisSacrum.scraper
              , ParktheaterEindhoven.scraper
              , RotterdamseSchouwburg.scraper
              , SSBA.scraper
              , SSBG.scraper
              , SSBU.scraper
              , TivoliVredenburg.scraper]

-- | Runs all the scrapers in 'scrapers' and stores their results in the local
--   database.
scrape :: [Scraper] -> IO ()
scrape scrapers = do
    putStrLn "Opening database"
    bracket (openLocalState def)
            (\acid -> do createCheckpointAndClose acid
                         putStrLn "Database was closed due to an exception")
            (\acid -> mapM_ (runScraper acid) scrapers)

withDb :: (AcidState Database -> IO a) -> IO ()
withDb f = bracket (openLocalState def)
                   (\acid -> do createCheckpointAndClose acid
                                putStrLn "Database was closed")
                   (\acid -> void $ f acid)

-- | Run a scraper at a specific url
test :: ScraperAction a -> T.Text -> IO a
test scraper url =
    do storeCookiesFrom (T.unpack url)

       tags <- downloadTags url
       let acid = error "No database connection available"
       let config           = ScraperConfig "test" (T.unpack url) tags acid

       (eitherX, messages) <- runScraperAction config scraper
       case eitherX of
           Right x -> return x
           Left x -> error x



-- * Database querying

-- | Search the database
search :: T.Text -> IO ()
search str =
    do bracket (openLocalState def)
               (createCheckpointAndClose)
               (\acid -> do matches  <- query acid (QTitle [str])
                            mapM_ (T.putStrLn . unwrap . title) matches)

       return ()

list :: IO ()
list =
    do bracket (openLocalState def)
               (createCheckpointAndClose)
               (\acid -> do matches  <- query acid QAll
                            mapM_ (T.putStrLn . unwrap . title) matches)

       return ()

-- | Store a list of entries in the database
store :: AcidState Database -> [Event] -> IO ()
store acid entries = forM_ entries $ \entry -> update acid (InsertEvent entry)