{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.SSBA where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types

import Control.DeepSeq
import Control.Exception

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = atUrl url $ do
    skipTo (TagOpen "div" [("class","main-content ")])
    repeatInContent $ do
        skipTo (TagOpen "a" [("class", "program-item")])
        takeAttribute "href"

    where -- Suffices for the first 1400 events
          url = "http://stadsschouwburgamsterdam.nl/voorstellingen/filter?page=100"



scrapeTitle :: ScraperAction Text
scrapeTitle =
    do skipTo (TagOpen "div" [("class", "title")])
       skipTo (TagOpen "h1" [])
       takeText

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes =
    do skipTo (TagOpen "section" [("class", "tickets-box")])
       repeatInContent (do skipTo (TagOpen "div" [("class", "day")])
                           d <- takeText
                           date <- detectPartialDate d
                           skipTo(TagOpen "div" [("class", "time")])
                           t <- takeText
                           time <- detectTime t
                           return (date, [time]))


scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagOpen "div" [("class", "content-box i18n-nl")])
       takeNestedText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices =
    do skipTo (TagOpen "div" [("class", "popup-holder")])
       skipTo (TagOpen "tbody" [])
       repeatInContent
           (do skipTo (TagOpen "td" [("class", "price")])
               text <- takeText
               detectPrice text)


scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipToText "Uitvoerenden:"
       skipTo (TagOpen "dd" [])
       text <- takeText
       splitEnumeration text


scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "img" [("class", "print_image")])
       text <- takeAttribute "src"
       saveImage text
-- "vierkant" met ctrlF levert link op. die met TakeAttribute pakken.

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Noord-Holland",
                  _town     = Town "Ansterdam",
                  _venue    = Venue "Stadsschouwburg Amsterdam"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription

scraper :: Scraper
scraper = Scraper
          {
            name      = "SSBA"
          , startUrl  = "http://stadsschouwburgamsterdam.nl/voorstellingen/filter"
          , scrapeEntries = simpleScrapeAll scrapeEntryUrls scrapeEvent
          }