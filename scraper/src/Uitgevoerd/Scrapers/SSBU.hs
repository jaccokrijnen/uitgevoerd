{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.SSBU where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)
import Control.Applicative
import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do skipTo (TagOpen "form" [("id","maandSelectForm")])
       months <- repeatInContent $ do
           skipTo (TagOpen "option" [])
           takeAttribute "value"
       -- p=-1 indicates to show all results
       let urls = map ("/voorstellingen/?t=m&d=1&p=-1&my=" <>) months

       concat <$> atUrls urls scrapeEntryUrlsMonth

-- | Scrapes the urls on the overview page of a specific month
scrapeEntryUrlsMonth :: ScraperAction [Text]
scrapeEntryUrlsMonth = do
    skipTo (TagOpen "html" [])
    repeatInContent $ do
        skipTo (TagOpen "div" [("class","info")])
        skipTo (TagOpen "a" [])
        takeAttribute "href"


scrapeTitle :: ScraperAction Text
scrapeTitle =
    do skipTo (TagOpen "div" [("id", "detailDates")])
       skipTo (TagOpen "h1" [])
       takeText

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes =
    do skipTo (TagOpen "div" [("id", "detailDates")])
       skipTo (TagOpen "tbody" [])
       repeatInContent (do skipTo (TagOpen "td" [("class", "datum")])
                           d <- takeText
                           date <- detectDate d
                           skipTo (TagOpen "td" [("class", "aanvang")])
                           t <- takeText
                           time <- detectTime t
                           return (date, [time]) )

scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagOpen "div" [("class", "txtBlock lang-nl")])
       takeNestedText

scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices =
    do  skipTo (TagOpen "div" [("id", "detailDates")])
        skipTo (TagOpen "td" [("class", "prijs")])
        skipTo (TagOpen "span" [("class", "help")])
        text <- takeAttribute "title"
        textlist <- splitEnumeration text
        mapM detectPrice textlist

scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipTo (TagOpen "div" [("id", "detailDates")])
       skipTo (TagOpen "span" [("class", "artiest")])
       t <- takeText
       return [t]

scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "meta" [("property", "og:image")])
       text <- takeAttribute "content"
       saveImage text


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Utrecht",
                  _town     = Town "Utrecht",
                  _venue    = Venue "Stadsschouwburg Utrecht"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription

scraper :: Scraper
scraper = Scraper
          {
            name      = "SSBU"
          , startUrl  = "https://www.stadsschouwburg-utrecht.nl/voorstellingen/"
          , scrapeEntries = simpleScrapeAll scrapeEntryUrls scrapeEvent
          }