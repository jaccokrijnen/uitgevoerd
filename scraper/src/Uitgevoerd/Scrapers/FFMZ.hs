{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.FFMZ where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
    skipTo (TagOpen "div" [("class", "details")])
    skipTo (TagOpen "h1" [])
    takeText


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagOpen "div" [("class", "details")])
    skipTo (TagOpen "span" [])
    t <- takeText
    date <- detectDate t

    skipTo (TagOpen "div" [])
    time <- takeText >>= detectTime

    return [(date, [time])]


scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("class","content")])
    takeNestedText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    skipTo (TagOpen "span" [("class", "prices")])
    repeatInContent $ do
        skipTo (TagOpen "span" [])
        takeText >>= detectPrice


scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "div" [("class", "details")])
    skipTo (TagOpen "em" [])
    t <- takeText
    return [t]


scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "div" [("class","thumbs")])
    skipTo (TagOpen "img" [])
    takeAttribute "src"

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Noord-Brabant",
                  _town     = Town "Eindhoven",
                  _venue    = Venue "Frits Filips Muziekgebouw"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = do

       -- Finds the amount of pages with events
       skipTo (TagOpen "ul" [("class","paginering")])
       skipTo (TagOpen "li" [("class","MarkupPagerNavLast MarkupPagerNavLastNum")])
       t <- takeText
       n <- detectNumber t

       let urls = map (\x -> "https://www.muziekgebouweindhoven.nl/concerten/page" <> (T.pack . show) x) [1..n]


       urlsPerPage <- atUrls urls $ do
          skipTo (TagOpen "div" [("class", "shows")])
          repeatInContent $ do
              skipTo (TagOpen "div" [("class", "show")])
              skipTo (TagOpen "a" [])
              takeAttribute "href"

       return (concat urlsPerPage)

-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "Frits Filips Muziekgebouw"
          , startUrl  = "https://www.muziekgebouweindhoven.nl/concerten/"
          , scrapeEntries = scrapeAll
          }

scrapeAll = simpleScrapeAll scrapeEntryUrls scrapeEvent


