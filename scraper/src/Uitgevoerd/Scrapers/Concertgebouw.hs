{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.Concertgebouw where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types


-- http://concertgebouw.nl/concerten-en-tickets
scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = do

    -- Find the number of concerts
    skipTo (TagOpen "div" [("id", "resulttoggle")])
    skipTo (TagOpen "span" [])
    t <- takeText
    n <- detectNumber t
    let url = "/concerten-en-tickets/per-pagina=" <> T.pack (show n)

    -- Go to the overview at specific page that shows them all.
    atUrl url $ do
        skipTo (TagOpen "html" [])
        repeatInContent $ do
            skipTo (TagOpen "div" [("class","readmore")])
            skipTo (TagOpen "a" [])
            takeAttribute "href"

scrapeTitle :: ScraperAction Text
scrapeTitle  =
    do skipTo (TagOpen "div" [("class", "centered")])
       skipTo (TagOpen "h1"[])
       takeText

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes =
    do skipTo (TagOpen "ul" [("class", "agenda reset")])
       repeatInContent $ do skipTo (TagOpen "div" [("class", "left")])
                            skipTo (TagOpen "h3" [])
                            text <- takeText
                            d <- detectDate text
                            skipTo (TagOpen "p" [])
                            t <- takeText
                            time <- detectTime t
                            return (d, [time] )

scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagOpen "div" [("id", "tab-1")])
       takeNestedText

scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipTo (TagOpen "div" [("id","tab-2")])
       skipTo (TagOpen "div" [("class", "column")])
       performers <- repeatInContent (do skipTo (TagOpen "span" [])
                                         takeText)
       return performers

scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "meta" [("property","og:image")])
       text <- takeAttribute "content"
       saveImage text

scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices =
    do skipTo (TagOpen "div" [("id", "tab-3")])
       skipTo (TagOpen "table" [("class", "prijsinfo")])
       pricelist <- repeatInContent
                         (do skipTo (TagOpen "tr" [])
                             skipTo (TagOpen "td" [])
                             skip 1
                             skipTo (TagOpen "td" [])
                             text <- takeText
                             detectPrice text)
       return pricelist


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Noord-Holland",
                  _town     = Town "Amsterdam",
                  _venue    = Venue "Het Concertgebouw"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription

scraper :: Scraper
scraper = Scraper
          {
            name      = "Concertgebouw"
          , startUrl  = "http://www.concertgebouw.nl/concerten-en-tickets"
          , scrapeEntries = simpleScrapeAll scrapeEntryUrls scrapeEvent
          }
