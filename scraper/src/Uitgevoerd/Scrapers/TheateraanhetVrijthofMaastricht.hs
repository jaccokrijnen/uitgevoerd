{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.TheateraanhetVrijthofMaastricht where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
    skipTo (TagOpen "input" [("type", "hidden")])
    takeAttribute "p-name"


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagOpen "h4" [("class", "eventtime")])
    text <- takeText
    date <- detectDate text
    time <- detectTime text
    return [((date), [(time)])]

scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("style", "position:relative;")])
    skipTo (TagOpen "h3" [])
    takeText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    skipTo (TagOpen "input" [("type", "hidden")])
    text <- takeAttribute "p-price"
    price <- detectPrice text
    return [price]


scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "input" [("type", "hidden")])
    text <- takeAttribute "p-group"
    return [text]

scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "input" [("type", "hidden")])
    takeAttribute "p-img"

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Limburg",
                  _town     = Town "Maastricht",
                  _venue    = Venue "Theater aan het Vrijthof"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do undefined


-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "Theater aan het vrijthof"
          , startUrl  = "http://www.theateraanhetvrijthof.nl/agenda/steekwoord/"
          , scrapeEntries = scrapeAll
          }

scrapeAll = simpleScrapeAll scrapeEntryUrls scrapeEvent


