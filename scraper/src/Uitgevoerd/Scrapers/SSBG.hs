{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.SSBG where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
    skipTo (TagOpen "meta" [("property", "og:title")])
    takeAttribute "content"


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagText "Datum")
    skipTo (TagOpen "dd" [])
    t <- takeText
    date <- detectDate t

    skipTo (TagText "Tijdstip")
    skipTo (TagOpen "dd" [])
    t <- takeText
    time <- detectTime t

    return [(date, [time])]



scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("class","event-content__dutch")])
    takeNestedText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    skipTo (TagOpen "div" [("id","pricing")])
    skipTo (TagOpen "tbody" [])
    repeatInContent $ do
        skipTo (TagOpen "td" [])
        t <- takeText
        detectPrice t



scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "strong" [("class", "event-header__extra")])
    t <- takeText
    return [t]


scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "meta" [("property", "og:image")])
    takeAttribute "content"

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Groningen",
                  _town     = Town "Groningen",
                  _venue    = Venue "Stadsschouwburg Groningen De Oosterpoort"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = do
    skipTo (TagOpen "span" [("class","page-numbers dots")])
    skipTo (TagOpen "a" [])
    numberText <- takeText
    n <- detectNumber numberText

    let pages = map (\x -> "http://www.de-oosterpoort.nl/programma/page/" <> (T.pack . show) x) [1..n]

    pagesUrls <- atUrls pages $ do
       skipTo (TagOpen "div" [("class", "destination__grid")])
       repeatInContent $ do
           skipTo (TagOpen "article" [])
           skipTo (TagOpen "a" [])
           takeAttribute "href"

    return (concat pagesUrls)






-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "De Doelen"
          , startUrl  = "http://www.de-oosterpoort.nl/programma/"
          , scrapeEntries = scrapeAll
          }

scrapeAll = simpleScrapeAll scrapeEntryUrls scrapeEvent


