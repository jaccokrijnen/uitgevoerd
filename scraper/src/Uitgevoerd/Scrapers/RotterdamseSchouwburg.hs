{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.RotterdamseSchouwburg where

import Control.Applicative
import Data.Maybe

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction

import Data.Acid
import Data.Acid.Advanced hiding (Event)


scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = do
    skipTo (TagOpen "select" [("id","maandForm-maand")])
    monthUrls <- repeatInContent $ do
        skipTo (TagOpen "option" [])
        val <- takeAttribute "value"
        return ("http://www.rotterdamseschouwburg.nl/voorstellingen/?maand=" <> val)

    -- Drop the first to avoid duplicates, it is "alle maanden",
    -- which is the current month, and is also listed.
    eventUrls <- atUrls (drop 1 monthUrls) $ do
        skipTo (TagOpen "html" [])
        repeatInContent $ do
            skipTo (TagOpen "p" [("class", "titel")])
            skipTo (TagOpen "a" [])
            url <- takeAttribute "href"
            toAbsoluteUrl url

    return (concat eventUrls)


scrapeTitle :: ScraperAction Text
scrapeTitle =
    do skipTo (TagOpen "p" [("class", "show-titel")])
       takeText


scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipTo (TagOpen "p" [("class", "show-artist")])
       text <- takeText
       performers <- splitEnumeration text
       return performers

scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagOpen "meta" [("property", "og:description")])
       takeAttribute "content"


scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "meta" [("property","og:image")])
       url <- takeAttribute "content"
       saveImage url

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
      skipTo (TagOpen "ul" [("class", "show-tabel")])
      repeatInContent $ do skipTo (TagOpen "li" [("class", "show-tabel-date")])
                           text <- takeText
                           d <- detectPartialDate text
                           skipTo (TagOpen "li"[("class", "show-tabel-time hover")])
                           t <- takeText
                           time <- detectTime t
                           return (d, [time] )

scrapePrices :: ScraperAction [(Int,Int)]
scrapePrices =
    do skipTo (TagOpen "ul" [("class", "show-tabel")])
       lists <- repeatInContent $ do
                    skipTo (TagOpen "li" [("class", "alwaysShow show-btn allowToWishlist")])
                    skipTo (TagOpen "ul" [("class", "hideList")])
                    skipTo (TagOpen "ul" [])
                    repeatInContent $do skipTo (TagOpen "li"[])
                                        text <- takeText
                                        p <- detectPrice text
                                        return (p)
       return (concat lists)


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Zuid-Holland",
                  _town     = Town "Rotterdam",
                  _venue    = Venue "Rotterdamse Schouwburg"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription

scraper :: Scraper
scraper = Scraper
          {
            name      = "RotterdamseSchouwburg"
          , startUrl  = "http://www.rotterdamseschouwburg.nl/voorstellingen/"
          , scrapeEntries = simpleScrapeAll scrapeEntryUrls scrapeEvent
          }