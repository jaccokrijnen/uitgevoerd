{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.MusisSacrum where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
   skipTo (TagOpen "span" [("class", "title")])
   takeText


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagOpen "h2" [("class", "noscr")])
    skipTo (TagOpen "div" [("class", "startDate")])
    d <- takeText
    datum <- detectDate d
    skipTo (TagOpen "div" [("class", "time")])
    t <- takeText
    tijd <- detectTime t
    return [((datum), [tijd])]


scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("id", "eventDescription")])
    skipTo (TagOpen "p" [])
    skip 1
    skipTo (TagOpen "p" [])
    takeText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    skipTo (TagOpen "div" [("class", "ticketPrices")])
    skipTo (TagOpen "table" [])
    repeatInContent $ do
         skipTo (TagOpen "span" [])
         text <- takeText
         detectPrice text


scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "span" [("itemprop", "performer")])
    text <- takeText
    return [text]


scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "div" [("class", "royalSlider rsDefault")])
    skipTo (TagOpen "img" [])
    takeAttribute "src"


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Gelderland",
                  _town     = Town "Arnhem",
                  _venue    = Venue "Musis Sacrum"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do undefined


-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "De Doelen"
          , startUrl  = "http://www.dedoelen.nl/_rss/rss.php?type=voorstellingen"
          , scrapeEntries = scrapeAll
          }

scrapeAll = undefined --simpleScrapeAll scrapeEntryUrls scrapeEvent


