{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.DeDoelen where

import Control.Applicative
import Data.Maybe

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction

import Data.Acid
import Data.Acid.Advanced hiding (Event)



-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle =
    do skipTo (TagOpen "h1" [("class", "title")])
       skip 1
       skipTo (TagOpen "span" [("class", "main")])
       main <- takeText
       skipTo (TagOpen "span" [("class", "sub")])
       sub  <- takeText
       return (main <> " - " <> sub)

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes =
    do skipTo (TagOpen "dl" [("class", "props")])
       repeatInContent $ do skipTo (TagOpen "dd" [("class", "date")])
                            text <- takeText
                            d <- detectDate text
                            skipTo (TagOpen "dd" [("class", "start")])
                            t <- takeText
                            time <- detectTime t
                            return (d, [time] )

scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagText "Omschrijving")
       skip 3
       takeNestedText

scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices =
    do skipTo (TagOpen "div" [("class", "content text")])
       skipTo (TagOpen "tbody" [])
       repeatInContent $ do skipTo (TagOpen "td" [("class", "price")])
                            text <- takeText
                            p <- detectPrice text
                            return (p)


scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipTo (TagOpen "div" [("id","performers")])
       skipTo (TagOpen "div" [])
       performers <- repeatInContent (do skipTo (TagOpen "strong" [])
                                         takeText)
       return performers


scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "meta" [("property","og:image")])
       url <- takeAttribute "content"
       saveImage url


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Zuid-Holland",
                  _town     = Town "Rotterdam",
                  _venue    = Venue "De Doelen"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription



-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do liftIO $ storeCookiesFrom "http://www.dedoelen.nl/nl/concerten/agenda/6548/corendon_presenteert/rtff_ferhat_gocer_husnu_senlendirici_concert/" -- Does not serve the cookies at the rss page...
       skipTo (TagOpen "rss" [])
       repeatInContent $ do skipTo (TagOpen "item" [])
                            skipTo (TagOpen "link" [])
                            skip 1
                            takeText


-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "De Doelen"
          , startUrl  = "http://www.dedoelen.nl/_rss/rss.php?type=voorstellingen"
          , scrapeEntries = scrapeAll
          }

scrapeAll :: ScraperAction [Event]
scrapeAll =
    do urls <- scrapeEntryUrls
       let amount = length urls
       liftIO $ putStrLn ("Found " ++ show amount ++ " event urls")
       coupledLoop urls 0 amount
       --mbs <- mapM (\url -> try $ atUrl url scrapeInfo) urls
       --return (catMaybes mbs)
    where coupledLoop []         n amount = return []
          coupledLoop (url:urls) n amount = do liftIO $ putStrLn ("[" ++ show n ++ "/" ++ show amount ++ "]")
                                               liftIO $ putStrLn (T.unpack url)
                                               mb <- try (atUrl url scrapeEvent)
                                               case mb of
                                                   Nothing    -> do liftIO $ putStrLn "failed"
                                                                    coupledLoop urls (n+1) amount
                                                   Just entry -> do liftIO $ putStrLn "success"
                                                                    liftIO (print entry)
                                                                    entries <- coupledLoop urls (n+1) amount
                                                                    return (entry : entries)




-- * TESTING STUFF

-- Testing for memory usage
scrapeIO :: AcidState Database -> IO ()
scrapeIO acid =
    do let url = (startUrl scraper)
       tags <- downloadTags (T.pack url)
       let config = ScraperConfig "IO Testing" url tags acid

       (eUrls, msgs) <- runScraperAction config scrapeEntryUrls

       case eUrls of
           (Right urls) -> do let amount = length urls
                              putStrLn ("Found " ++ show amount ++ " event urls")
           (Left  _)    -> return ()


    --   coupledLoop urls 0 amount
    --   --mbs <- mapM (\url -> try $ atUrl url scrapeInfo) urls
    --   --return (catMaybes mbs)
    --where coupledLoop []         n amount = return []
    --      coupledLoop (url:urls) n amount = do liftIO $ putStrLn ("[" ++ show n ++ "/" ++ show amount ++ "]")
    --                                           liftIO $ putStrLn (T.unpack url)
    --                                           mb <- try (atUrl url scrapeInfo)
    --                                           case mb of
    --                                               Nothing    -> do liftIO $ putStrLn "failed"
    --                                                                coupledLoop urls (n+1) amount
    --                                               Just entry -> do liftIO $ putStrLn "success"
    --                                                                liftIO (print entry)
    --                                                                entries <- coupledLoop urls (n+1) amount
    --                                                                return (entry : entries)


scrapeN :: Int -> ScraperAction [Event]
scrapeN n | n <= 0    = return []
          | otherwise =
    do urls <- scrapeEntryUrls
       mbs <- mapM (\url -> try $ atUrl url scrapeEvent) (take n urls)
       return (catMaybes mbs)