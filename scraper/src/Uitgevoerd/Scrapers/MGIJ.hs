{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.MGIJ where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types

import Control.DeepSeq
import Control.Exception

scrapeAll = simpleScrapeAll scrapeEntryUrls scrapeEvent

scraper :: Scraper
scraper = Scraper
          {
            name      = "Muziekgebouw aan 't IJ"
          , startUrl  = "https://www.muziekgebouw.nl/agenda/Concerten?p=1"
          , scrapeEntries = scrapeAll
          }

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls = loop 1

    where loop n = do
              urls <- scrapeSinglePage n
              liftIO $ evaluate (force urls) -- avoid memory leak

              if null (force urls)
                  then do
                      return urls
                  else do
                      urls' <- loop (n+1)
                      return (urls ++ urls')

scrapeSinglePage n = atUrl (prefix <> (T.pack . show) n) $ do
    skipTo (TagOpen "html" [])
    repeatInContent $ do
        skipTo (TagOpen "h3" [("class", "voorstellingKop")])
        skipTo (TagOpen "a" [])
        takeAttribute "href"

prefix = "https://www.muziekgebouw.nl/agenda/Concerten?p="


scrapeTitle :: ScraperAction Text
scrapeTitle =
    do skipTo (TagOpen "div" [("id", "showHead")])
       skipTo (TagOpen "span" [])
       takeText

scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes= do
    skipTo (TagOpen "div" [("id", "showHead")])
    repeatInContent $ do skipTo (TagOpen "span" [("class", "date")])
                         text <- takeText
                         date <- detectDate text
                         time <- detectTime text
                         return (date, [time])

scrapeDescription :: ScraperAction Text
scrapeDescription =
    do skipTo (TagOpen "div" [("id", "showDescription")])
       takeNestedText

scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices =
    do skipTo (TagOpen "dl" [("class", "columnsAlt")])
       repeatInContent (do skipTo (TagOpen "dt" [])
                           text <- takeText
                           detectPrice text)


scrapePerformers :: ScraperAction [Text]
scrapePerformers =
    do skipTo (TagOpen "div" [("id", "tabs-uitvoerenden")])
       skipTo (TagOpen "p"[])
       repeatInContent (do skipTo (TagOpen "b" [])
                           text <- takeText
                           return text)


scrapeImage :: ScraperAction Text
scrapeImage =
    do skipTo (TagOpen "img" [("class", "rsImg imgTooltip")])
       text <- takeAttribute "src"
       saveImage text


scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Noord-Holland",
                  _town     = Town "Amsterdam",
                  _venue    = Venue "Muziekgebouw aan 't IJ"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription