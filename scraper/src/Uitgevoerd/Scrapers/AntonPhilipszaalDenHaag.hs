{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.AntonPhilipszaalDenHaag where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
    skipTo (TagOpen "span" [("class", "title")])
    takeText


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagOpen "div" [("class", "startDate")])
    text <- takeText
    date <- detectDate text
    skipTo (TagOpen "div" [("class", "time")])
    text <- takeText
    time <- detectTime text
    return [((date), [(time)])]


scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("id", "eventDescription")])
    skipTo (TagOpen "p" [("class", "justifyLeft")])
    takeText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    undefined
    --geen prijsinformatie op de overzichtspagina. Komt pas op de bestelpagina nar voren. --




scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "span" [("class", "subtitle")])
    t <- takeText
    return [t]


scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "div" [("id", "programmeDetail")])
    skipTo (TagOpen "img" [])
    takeAttribute "src"

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Zuid-Holland",
                  _town     = Town "Den Haag",
                  _venue    = Venue "Anton Philipszaal"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do undefined


-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "Anton Philipszaal"
          , startUrl  = "https://www.ldt.nl/programma/"
          , scrapeEntries = scrapeAll
          }

scrapeAll = simpleScrapeAll scrapeEntryUrls scrapeEvent


