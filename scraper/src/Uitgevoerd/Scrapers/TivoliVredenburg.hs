{-#LANGUAGE OverloadedStrings #-}
module Uitgevoerd.Scrapers.TivoliVredenburg where

import Text.HTML.TagSoup
import qualified Data.Text as T
import Data.Text (Text)

import Uitgevoerd.Scraper
import Uitgevoerd.Database.Types
import Control.Applicative


-- * Scrapers that work on the event page

scrapeTitle :: ScraperAction Text
scrapeTitle = do
    skipTo (TagOpen "div"[("class", "contentLayer eventDetail")])
    skipTo (TagOpen "span" [("itemprop", "name")])
    takeText


scrapeDateTimes :: ScraperAction [((Int, Int, Int), [(Int, Int)])]
scrapeDateTimes = do
    skipTo (TagOpen "section" [("class", "infoPane classical")])
    skipTo (TagOpen "time" [("class", "startDate")])
    d <- takeText
    date <- detectDate d
    skipTo (TagOpen "div"[("class", "detail")])
    skipTo (TagOpen "div" [("class", "detailValue")])
    t <- takeText
    time <- detectTime t
    return [(date, [time])]


scrapeDescription :: ScraperAction Text
scrapeDescription = do
    skipTo (TagOpen "div" [("class", "description")])
    takeText


scrapePrices :: ScraperAction [(Int, Int)]
scrapePrices = do
    skipTo (TagOpen "span" [("class", "price")])
    p <- takeText
    price <- detectPrice p
    return [price]

scrapePerformers :: ScraperAction [Text]
scrapePerformers = do
    skipTo (TagOpen "div"[("class", "additionalInfo")])
    skipTo (TagOpen "div"[("class", "detailValue")])
    repeatInContent
            (do skipTo (TagOpen "strong" [])
                takeText)

scrapeImage :: ScraperAction Text
scrapeImage = do
    skipTo (TagOpen "div" [("class", "fullImage")])
    skipTo (TagOpen "img" [])
    takeAttribute "src"

scrapeLocation :: ScraperAction Location
scrapeLocation =
    do return Location {
                  _province = Province "Utrecht",
                  _town     = Town "Utrecht",
                  _venue    = Venue "TivoliVredenburg"
              }

scrapeEvent :: ScraperAction Event
scrapeEvent = simpleScrapeEvent scrapeTitle
                                scrapeDateTimes
                                scrapeImage
                                scrapeLocation
                                scrapePerformers
                                scrapePrices
                                scrapeDescription


-- * Scrapers for the overview pages

scrapeEntryUrls :: ScraperAction [Text]
scrapeEntryUrls =
    do undefined


-- * Complete scraper definition

scraper :: Scraper
scraper = Scraper
          {
            name      = "TivoliVredenburg"
          , startUrl  = "https://www.tivolivredenburg.nl/nl/?category=classical#agenda"
          , scrapeEntries = scrapeAll
          }

scrapeAll = undefined -- simpleScrapeAll scrapeEntryUrls scrapeEvent


