{-#LANGUAGE OverloadedStrings, GADTs, KindSignatures #-}
module Uitgevoerd.Scraper.ScraperAS where

import Control.Applicative

import qualified Data.Text as T
import Data.Text (Text)

import Text.HTML.TagSoup
import Uitgevoerd.Scraper.ScraperAction

-- Abstract Syntax

data ScraperAS :: * -> * where
    Skip           :: Int -> ScraperAS ()
    SkipWhile      :: (Tag Text -> Bool) -> ScraperAS ()
    SkipTo         :: Tag Text -> ScraperAS ()
    TakeTag        :: ScraperAS (Tag Text)
    TakeText       :: ScraperAS Text
    TakeNestedText :: ScraperAS Text
    Try            :: ScraperAS a -> ScraperAS (Maybe a)
    UseAllTags     :: ScraperAS a -> ScraperAS a

    Fmap           :: (a -> b) -> ScraperAS a -> ScraperAS b

    Star           :: ScraperAS (a -> b) -> ScraperAS a -> ScraperAS b
    Pure           :: a -> ScraperAS a

    Bind           :: ScraperAS a -> (a -> ScraperAS b) -> ScraperAS b


    -- AtUrl

instance Functor ScraperAS where
    fmap = Fmap

instance Applicative ScraperAS where
    pure  = Pure
    (<*>) = Star

instance Monad ScraperAS where
    (>>=)  = Bind
    return = Pure

-- | The interpreter semantics
toScraperAction :: ScraperAS a -> ScraperAction a
toScraperAction (Skip n)           = skip n
toScraperAction (SkipWhile p)      = skipWhile p
toScraperAction (SkipTo tag)       = skipTo tag
toScraperAction TakeTag            = takeTag
toScraperAction TakeText           = takeText
toScraperAction TakeNestedText     = takeNestedText
toScraperAction (Try as)           = try (toScraperAction as)
toScraperAction (UseAllTags as)    = useAllTags (toScraperAction as)

toScraperAction (Fmap f as)        = fmap f (toScraperAction as)

toScraperAction (Star asf asx)     = (toScraperAction asf) <*> (toScraperAction asx)
toScraperAction (Pure x)           = pure x

toScraperAction (Bind as as2)      = (toScraperAction as) >>= (\x -> toScraperAction (as2 x))
