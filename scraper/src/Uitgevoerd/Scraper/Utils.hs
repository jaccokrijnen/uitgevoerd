{-# LANGUAGE OverloadedStrings, RecordWildCards #-}
module Uitgevoerd.Scraper.Utils where


import Control.Exception

import Data.Maybe
import Data.Monoid
import Data.Char
import Data.Acid

import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as B (pack)
import qualified Data.ByteString as B hiding (pack)
import qualified Data.ByteString.Lazy as B (toStrict)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.Encoding.Error as E
import Data.Time
import Data.Time.Clock
import Data.Hashable

import Text.HTML.TagSoup as TagSoup hiding (maybeTagText)
import Text.Printf

import Control.Monad
import Control.Applicative
import Control.DeepSeq



import Network
import Network.Browser hiding (Cookie)
import Network.HTTP
import Network.HTTP.Conduit
import Network.URI hiding (query)

import System.FilePath
import System.Directory

import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction



type Tags = [Tag T.Text]

-- * Web browsing


-- | Given an url, downloads the contents as a bytestring
downloadBs :: String
           -> IO B.ByteString
downloadBs url = withSocketsDo $ do
    request' <- parseUrl url
    jar <- readCookieJar
    let request = request' {cookieJar = Just jar} -- set custom cookies
    res <- withManager $ httpLbs request
    return (B.toStrict $ responseBody res)



-- | Adds new cookies received at the url to the cookiejar on disk and returns the new jar
storeCookiesFrom :: String -> IO CookieJar
storeCookiesFrom url = do
    new <- downloadCookies url
    putStrLn ("new cookies for cookiejar: " ++ show new)
    jar <- readCookieJar
    let jar' = jar <> new
    writeFile "cookiejar" (show jar')
    return jar'


-- | Reads the headers of a given url and extracts the cookies
downloadCookies :: String -> IO CookieJar
downloadCookies url = withSocketsDo $ do
    request <- parseUrl url
    res <- withManager $ httpLbs request
    return (responseCookieJar res)

-- | Reads the cookiejar from disk
readCookieJar :: IO CookieJar
readCookieJar = do
    b <- doesFileExist "cookiejar"
    unless b $ do
        writeFile "cookiejar" (show (createCookieJar []))

    contents <- readFile "cookiejar"
    evaluate (force contents)
    return (read contents)


-- | Requests the url and gets the body as a 'Text'
download :: String -> IO T.Text
download x = E.decodeUtf8With E.lenientDecode <$> downloadBs x

-- | Requests the url and get the body as 'Tags'
downloadTags :: T.Text -> IO Tags
downloadTags str = parseTags <$> download (T.unpack str)

-- | Raw request of which the response body is saved to a given path relative
--   to the working directory
downloadAndSaveAs :: Text -> String -> IO FilePath
downloadAndSaveAs url name =
    do raw <- downloadBs (T.unpack url)
       B.writeFile name raw
       dir <- getCurrentDirectory
       return $ name

-- | Downloads an image from an url and names it with a hash of the url
downloadThumb :: Text -> IO FilePath
downloadThumb url =
    do createDirectoryIfMissing True ("img" </> "thumbs")
       let hashed = abs (hash url)
       let name = "img" </> "thumbs" </> show hashed <.> takeExtension (T.unpack url)
       downloadAndSaveAs url name

-- | Downloads the page to "grabbed.htm", for analyzing a html page
grab :: String -> IO ()
grab str = do src <- download str
              T.writeFile "grabbed.htm" src



-----------------------------------------------------------------
-- Tags
-----------------------------------------------------------------

-- | Parses tags but cleans them by removing all whitespace that is not a normal space.
--   Also filters out the tags that contain only whitespace.
parseTags' :: T.Text -> [Tag T.Text]
parseTags' t = force (filter nonEmpty . map clean . (TagSoup.parseTags) $ t)
    where nonEmpty (TagText t) = not (T.null t)
          nonEmpty _           = True
          clean (TagText t)    = let t' = T.filter (\c -> c == ' ' || not (isSpace c)) t -- filters out whitespace characters except normal space
                                 in  TagText t'
          clean t              = t

-- | Obsolete
maybeTagText :: [Tag T.Text] -> Maybe T.Text
maybeTagText (TagText str : _) = Just str
maybeTagText _                 = Nothing

-- | Obsolete
maybeFirstText :: Tags -> Maybe T.Text
maybeFirstText = maybeTagText . dropWhile (~/= TagText T.empty)


-- | Obsolete
allBodyText :: Tags -> Maybe T.Text
allBodyText (TagOpen parent _ : tags) = Just $ allBodyText' 0 False tags

          -- stack indicates the amount of tags passed with the same name as the original parent
          -- seenTag indicates whether any other tag than TagText was encountered (so a space can be inserted for it)
    where allBodyText' stack seenTag ((TagText t)     :tags)                  = let addSpace  = (if seenTag then (" " <>) else id)
                                                                                    cleanText = T.dropWhile isSpace t   -- ignore any space prefix
                                                                                    rest      = allBodyText' stack False tags
                                                                                in  addSpace (cleanText <> rest)
          allBodyText' 0     seenTag ((TagClose name) :tags) | name == parent = ""
          allBodyText' stack seenTag ((TagOpen name _):tags) | name == parent = allBodyText' (stack + 1) True tags
          allBodyText' stack seenTag ((TagClose name) :tags) | name == parent = allBodyText' (stack - 1) True tags
          allBodyText' stack seenTag (_               :tags)                  = allBodyText' stack True tags

allBodyText _ = Nothing

---------------------------
-- Required for deepseq
---------------------------

instance (NFData a) => NFData (Tag a) where
    rnf t = case t of
                TagOpen str args -> case rnf str of () -> rnf args
                TagClose str -> rnf str
                TagText str -> rnf str
                TagComment str -> rnf str
                TagWarning str -> rnf str
                TagPosition r c -> ()


-----------------------------------------------------------------
-- Running the pure scraper in IO
-----------------------------------------------------------------
{-
-- | Runs a complete agenda scraper
--   Also prints some stats about the success rate
runScraper :: AcidState Database -> Scraper -> IO ()
runScraper db scraper@(Scraper {..}) =
    do putStrLn $ "Running scraper: " ++ name


       -- Collect all event urls, then scrape every url
       urls    <- allEventUrls scraper
       entries <- forM urls $ \url ->
                    do tags <- fmap parseTags' (download url)
                       return $ scrapeEntry url tags



       let succesful  = catMaybes entries

       -- Check if the entry is a duplicate.
       -- If it is, it should replace its older version
       forM_ succesful (tryInsertEntry db)



       -- Log some statistics (failure is acceptable sometimes, e.g. a page is under construction)
       let lengthAttempted = length entries
       let lengthSuccesful = length succesful
       printf "%d Succesful, %d Failed \r\n" lengthSuccesful (lengthAttempted - lengthSuccesful)

       return ()

-- | Tries to insert the entry and also decide its ID
--   If succesfull, returns the entry with a valid id, otherwise just the entry itself
tryInsertEntry :: AcidState Database -> Entry -> IO Entry
tryInsertEntry db entry =
  do sameUrlEntries <- query db (QUrl (url entry))
     case sameUrlEntries of
         ([e]) -> do print "Overwrote an older version"
                     let existingId = entryId e
                     update db (OverwriteAt existingId entry {entryId = existingId})
                     return entry {entryId = existingId}

         []    -> do print "This entry was not yet in the database"
                     newId <- query db QNextId
                     update db (Insert entry {entryId = newId})
                     return entry {entryId = newId}

         _     -> do print ("The database contains multiple entries for url: " ++ show (url entry))
                     return entry

-- | Gets all the event-urls on the different agenda pages
allEventUrls :: Scraper -> IO [String]
allEventUrls scraper@(Scraper{..}) = getUrls' 0
    where getUrls' n =
              do tags     <- fmap parseTags' $ download (agendaUrlAt n)
                 let urls =  collectEventUrls  tags
                 let cont =  continueCollecting tags

                 -- deepseq: urls and cont have to be forced so that tags can be
                 -- garbage collected
                 rest          <- urls `deepseq` (if cont
                                                     then getUrls' (n+1)
                                                     else return [])
                 return $ urls ++ rest

----------------------------------------
-- Testing functions
----------------------------------------


-- | Scrapes a single event entry using the scraper at a given url
runEntryScraper :: Scraper -> AcidState Database -> String -> IO Entry
runEntryScraper Scraper{..} db url =
    do content  <- download url
       let tags = parseTags' content
       let mbEntry = scrapeEntry url tags

       entryWithId <- case mbEntry of
                        Just entry -> tryInsertEntry db entry
                        Nothing -> return (error "no entry here")

       return $ entryWithId
-}
