{-# LANGUAGE TupleSections, NoOverloadedStrings #-}
module Uitgevoerd.Scraper.SmartParsers where

import qualified Data.Text as T
import Data.Text (Text)
import Data.Char
import Data.List
import Data.Maybe
import Data.Time.Clock
import Data.Time.Calendar

import Uitgevoerd.Database.Types
import Uitgevoerd.Scraper.ScraperAction

import Control.Applicative

import qualified Text.ParserCombinators.UU.Core as UU
import Text.ParserCombinators.UU.Core ((<<|>))
import Text.ParserCombinators.UU.Idioms
import Text.ParserCombinators.UU.Derived
import Text.ParserCombinators.UU.Utils
import Text.ParserCombinators.UU.BasicInstances hiding (Parser)



---------------------------
-- * Utils
---------------------------
type Parser a = UU.P (Str Char T.Text LineCol) a

parse :: Parser a -> T.Text -> Maybe a
parse p input = case UU.parse ( (,) <$> p <*> UU.pEnd) (createStr (LineCol 1 1) input) of
                  (a,[]  ) -> Just a
                  (a,msgs) -> Nothing

parse' :: Parser a -> T.Text -> T.Text -> ScraperAction a
parse' p errorMessage input = case UU.parse ( (,) <$> p <*> UU.pEnd) (createStr (LineCol 1 1) input) of
                                  (x, []) -> return x
                                  (_,  _) -> throwError (T.unpack errorMessage)

match' :: Parser a -> Text -> Text -> ScraperAction a
match' p errorMessage input = case match p input of
                                  Just x  -> return x
                                  Nothing -> throwError (T.unpack errorMessage)

-- | Tries to fit the parser on all tails of the input
match :: Parser a -> Text -> Maybe a
match p input = listToMaybe . catMaybes $ map (parse $ p <* pMunch (const True)) ts
    where ts = T.tails input


noDigits :: Parser String
noDigits = pMany (pSatisfy (not . isDigit) (Insertion "" 'a' 20))

stripNbsp :: T.Text -> T.Text
stripNbsp = T.filter (/= '\160')

----------------------------
-- * Number
----------------------------
detectNumber :: T.Text -> ScraperAction Int
detectNumber t = match' parseNumber
                        (T.pack "Could not detect time in: " <> t)
                        (stripNbsp t)

parseNumber :: Parser Int
parseNumber = pNatural


----------------------------
-- * Time
----------------------------

-- | Tries to recognize a time, valid formats are:
--
-- > 12:30
-- > 12.30
-- > 12 uur
detectTime :: T.Text -> ScraperAction (Int, Int)
detectTime t = match' parseTime
                      (T.pack "Could not detect time in: " <> t)
                      (stripNbsp t)



parseTime :: Parser (Int, Int)
parseTime =  iI (,)  pNatural ':' pNatural Ii
         <|> iI (,)  pNatural '.' pNatural Ii
         <|> iI (,0) pNatural "uur" Ii




----------------------------
-- * Price
----------------------------

-- | Tries to recognize a price, valid formats are
--
-- > 8.50
-- > 8,50
-- > 20.-
-- > 20,-
-- > 10 €
-- > € 10
--
detectPrice :: T.Text -> ScraperAction (Int, Int)
detectPrice t = match' parsePrice
                       (T.pack "Could not detect price in: " <> t)
                       (stripNbsp t)
    where noPrice :: Parser [Char]
          noPrice = pMany (pSatisfy (\c -> (not (isDigit c))) (Insertion "" 'a' 20))

parsePrice :: Parser (Int, Int)
parsePrice =  iI (\  e c -> (e,c))       pNatural pSuffix Ii
          <|> iI (\_ e c -> (e,c)) pEuro pNatural (pSuffix <<|> pure 0) Ii

pEuro           = lexeme $ pAny pToken ["euro", "€"]
pNaturalOrMinus = pNatural <|> pure 0 <* pSym '-'
pSuffix :: Parser Int
pSuffix = pSeperator *> pNaturalOrMinus <|> pure 0 <* pEuro
pSeperator :: Parser Char
pSeperator      = pSym '.' <|> pSym ','



----------------------------
-- * Date
----------------------------

-- | Tries to recognize a date, valid formats are:
--
-- > 3/10/2014
-- > 3-10-2014
-- > 3 oktober 2014
-- > 3 okt 2014
detectDate :: T.Text -> ScraperAction Date
detectDate t = match' parseDate
                      (T.pack "Could not detect date in: " <> t)
                      (stripNbsp t)

parseDate :: Parser Date
parseDate =  iI constructDate pNatural "/" pNatural "/" pNatural Ii
         <|> iI constructDate pNatural "-" pNatural "-" pNatural Ii
         <|> iI constructDate pNatural pMonth pNatural Ii
    where
          -- pWeekDay   = pAny pToken (days ++ daysAbbrev)
          -- days       = ["maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"]
          -- daysAbbrev = ["ma", "di", "wo", "do", "vr", "za", "zo"]
          constructDate d m y = (y, m, d)


detectPartialDate :: T.Text -> ScraperAction Date
detectPartialDate t = do
    (m, d)    <- match' parseMonthDay (T.pack "Could not detect a partial date in: " <> t) t
    (yNow, mNow, dNow) <- (toGregorian . utctDay) <$> liftIO getCurrentTime
    let y = fromInteger $ if m > mNow then yNow else
                              if m < mNow then yNow+1
                                          else if d >= dNow then yNow else yNow + 1
    return (y, m, d)



parseMonthDay :: Parser (Int, Int)
parseMonthDay = iI constructDate pNatural "/" pNatural Ii
            <|> iI constructDate pNatural "-" pNatural Ii
            <|> iI constructDate pNatural pMonth Ii
    where constructDate d m = (m, d)

pMonth :: Parser Int
pMonth = lexeme $ month2nat <$> pAny pToken (months ++ monthsAbbrev)
    where month2nat m = case m of
                            "october" -> 10 -- special spelling
                            m         -> case elemIndex m months of
                                             Just x -> x + 1
                                             Nothing -> case elemIndex m monthsAbbrev of
                                                             Just x -> x + 1
                                                             Nothing -> error $ "Not a recognized month: " ++ m

          months = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"]
          monthsAbbrev = ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sept", "okt", "nov", "dec"]
