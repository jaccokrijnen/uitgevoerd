{-#LANGUAGE MultiParamTypeClasses, FlexibleInstances, OverloadedStrings, GADTs, ViewPatterns #-}

-- | This module contains a monadic approach to the scraping process.
-- It exports the 'ScraperAction a' type, together with an interface
-- for programming in it.
module Uitgevoerd.Scraper.ScraperAction (module Uitgevoerd.Scraper.ScraperAction,
                                        module Control.Monad.RWS.Strict,
                                        module Control.Monad.Except) where

import Data.Char
import Data.Acid
import Data.Maybe
import qualified Data.Text as T
import Data.Text (Text)
import Text.HTML.TagSoup

import Control.Arrow
import Control.Applicative
import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.Except
import Control.Monad.RWS.Strict

import System.IO

import Uitgevoerd.Database.Types
import Uitgevoerd.Scraper.Utils
import Uitgevoerd.Scraper.Logging

import Network.URI

--
-- * Types
--

-- | A computation type for scraping html
newtype ScraperAction a = ScraperAction {_runScraperAction :: ExceptT        String             -- Scrapers can fail
                                                             (RWST ScraperConfig             -- Reader: Access tags to parse and database connection
                                                                   ()                        -- Writer: nothing
                                                                   ([Message], Tags, Tags)   -- State : Stack of messages and current point in tags
                                                                       IO)                   -- perform IO: downloading additional pages/images etc.
                                                           a}


-- | The read-only data available to the scraper
data ScraperConfig = ScraperConfig { _name :: String              -- ^ name of the scraper
                                   , _url  :: String              -- ^ the current page url
                                   , _tags :: Tags                -- ^ The tags of the current page
                                   , _db   :: AcidState Database  -- ^ db connection
                                   }




-- * The interface for the ScraperAction

instance Functor ScraperAction where
    fmap f x = x >>= return . f

instance Applicative ScraperAction where
    pure    = return
    f <*> x = do _f <- f
                 _x <- x
                 return (_f _x)


instance Monad ScraperAction where
    return  = ScraperAction . return
    m >>= f = ScraperAction (_runScraperAction m >>= (_runScraperAction . f))

-- Trivial instance since the stack is already MonadReader
instance MonadReader ScraperConfig ScraperAction where
    ask = ScraperAction ask
    local f x = ScraperAction (local f (_runScraperAction x))
    reader f = ScraperAction  (reader f)

-- Trivial instance since the stack is already MonadReader
instance MonadState ([Message], Tags, Tags) ScraperAction where
    get     = ScraperAction get
    put x   = ScraperAction (put x)
    state f = ScraperAction (state f)


pushMessage :: Message -> ScraperAction ()
pushMessage msg = modify (\(msgs, back, forw) -> (msg:msgs, back, forw))

popMessage :: ScraperAction ()
popMessage = modify (\(msgs, back, forw) -> (tail msgs, back, forw))

getForward  :: ScraperAction Tags
getForward = (\(_,_,forward) -> forward) <$> get

getBackward :: ScraperAction Tags
getBackward = (\(_,backward,_) -> backward)  <$> get

instance MonadError String ScraperAction where

    -- Log the error and fail
    throwError str =
        do logFail str
           ScraperAction $ throwError str

    catchError m f =
        do let f' error = _runScraperAction (popMessage >> f error)  -- Pop the last message, it was caught
               m'       = _runScraperAction m
           ScraperAction $ catchError m' f'

instance MonadIO ScraperAction where
    liftIO = ScraperAction . liftIO


-- | Runs the scraper monad
runScraperAction :: ScraperConfig -> ScraperAction a -> IO (Either String a, [Message])
runScraperAction config scraper =
        let stackExcStateIO = (_runScraperAction scraper)
            stackRWSTIO     = runExceptT stackExcStateIO
            stackIO         = runRWST stackRWSTIO config ([], [], _tags config)
        in  do (val, (messages, _, _), ()) <- stackIO
               return (val, messages)


--
-- ** Error reporting
--

-- | Pushes an error on the error stack
logFail :: String -> ScraperAction ()
logFail str =
    do config <- ask
       tags   <- (\(_,_,coming) -> coming) <$> get
       let msg    = Message MessageFail (_url config) (_name config) tags str
       pushMessage msg

-- | Logs an error for this scraper
--logWarning :: String -> ScraperAction ()
--logWarning str =
--    do config <- ask
--       let msg = Message MessageWarning (_url config) (_name config) [str]
--       pushMessage msg

---- | Logs a normal message for this scraper
--logNormal :: String -> ScraperAction ()
--logNormal str =
--    do config <- ask
--       let msg = Message MessageNormal (_url config) (_name config) [str]
--       pushMessage msg




--
-- ** Parsing Tags
--

-- | Take the first tag from the input stream
takeTag :: ScraperAction (Tag Text)
takeTag =
    do (msgs, back, forw) <- get
       case forw of
           []     -> throwError "takeTag: no tags available"
           (t:ts) -> do put (msgs, t:back, ts)
                        return t

-- | Take the first 'TagText'
takeText :: ScraperAction Text
takeText =
    do skipWhile (~/= TagText T.empty) `catchError` (\_ -> throwError "skipTo: no more tags")
       tag <- takeTag
       case tag of
           TagText str -> return str
           _           -> throwError "takeText: no TagText available"

takeAttribute :: Text -> ScraperAction Text
takeAttribute attribute =
    do tag <- takeTag
       case tag of
           (TagOpen _ attrs) -> case lookup attribute attrs of
                                   Nothing  -> throwError $ "takeAttribute: The tag has no attribute named " ++ T.unpack attribute
                                   Just val -> return val
           _                 -> throwError "takeAttribute: No opening tag available"

-- | Repeats an action in the body of an opening tag.
--   Expects to start at an openingtag
--   Will skip failing parses
repeatInContent :: ScraperAction a -> ScraperAction [a]
repeatInContent action =
    do (errors, back, forw) <- get
       (content, rest)      <- contentTags forw
       put ([], [], content)

       xs <- local (\config -> config {_tags = content}) (getForward >>= repeated )

       put (errors, back, rest) -- TODO: back does not include the visited subtree
       return xs

    where repeated tagsPrev = do
              mbmb <- ((Just . Just) <$> action) `catchError` (\_ -> do
                                                                    tags <- getForward
                                                                    if tags == []
                                                                        then return Nothing
                                                                        else return (Just Nothing))


              -- Ugly nested Maybe's, if the outer is Nothing
              -- we reached the end, otherwise the inner tells about the
              -- result within the performed action

              case mbmb of
                  Nothing       -> return []
                  Just res      -> do
                      -- Check for infinite loop
                      tags <- getForward
                      when (tags == tagsPrev) (throwError "repeatInContent: Detected infinite loop, the action is not making progress")

                      case res of
                          Nothing  -> repeated tags
                          (Just x) -> do
                              xs <- repeated tags
                              return (x:xs)

-- | Returns the tags between the opening and closing tags
--
contentTags :: [Tag Text] -> ScraperAction ([Tag Text], [Tag Text])
contentTags ((TagOpen parent _) : tags) = return (go 0 tags)
    where -- n is the number of encountered opening tags with the
          -- same name as the parent node
          go n (t@(TagOpen name _):tags) | name == parent = first (t:) (go (n+1) tags)
          go 0 (t@(TagClose name) :tags) | name == parent = ([], tags)
          go n (t@(TagClose name) :tags) | name == parent = first (t:) (go (n-1) tags)
          go n (t:tags)                                   = first (t:) (go n     tags)
          go n []                                         = error ("No closing tag for <" ++ T.unpack parent ++ ">")
contentTags [] = throwError "repeatInContent: No more tags available"
contentTags _  = throwError "repeatInContent: Not at an openingstag"

-- | Has to start at an open tag, will then collect
--   all text until the matching closing tag
takeNestedText :: ScraperAction Text
takeNestedText =
    do tags <- getForward
       allBodyText tags

  where allBodyText :: Tags -> ScraperAction T.Text
        allBodyText (TagOpen parent _ : tags) =
            let -- stack indicates the amount of tags passed with the same name as the original parent
                -- seenTag indicates whether any other tag than TagText was encountered (so a space can be inserted for it)
                allBodyText' stack seenTag ((TagText t)     :tags)                  = let addSpace  = (if seenTag then (" " <>) else id)
                                                                                          cleanText = T.dropWhile isSpace t   -- ignore any space prefix
                                                                                          rest      = allBodyText' stack False tags
                                                                                      in  addSpace (cleanText <> rest)
                allBodyText' 0     seenTag ((TagClose name) :tags) | name == parent = ""
                allBodyText' stack seenTag ((TagOpen name _):tags) | name == parent = allBodyText' (stack + 1) True tags
                allBodyText' stack seenTag ((TagClose name) :tags) | name == parent = allBodyText' (stack - 1) True tags
                allBodyText' stack seenTag (_               :tags)                  = allBodyText' stack True tags

            in return $ allBodyText' 0 False tags

        allBodyText _                         = throwError "takeNestedText: First tag is not an open tag!"




-- | Skip n tags in the input stream
skip :: Int -> ScraperAction ()
skip n =
    do (msgs, back, forw) <- get
       let forw' = drop n forw
           back' = (reverse $ take n forw) ++ back
       put (msgs, back', forw')




-- | Skip tags while the predicate holds
skipWhile :: (Tag T.Text -> Bool) -> ScraperAction ()
skipWhile p =
  do (msgs, back, forw) <- get
     case forw of
         []     -> throwError "skipWhile: no more tags"
         (t:ts) | p t       -> do put (msgs, t:back, ts) -- move along a single tag
                                  skipWhile p
                | otherwise -> return ()


-- | Skip to the first occurence of a certain tag (using approximate matching from tagsoup).
--   For instance
--
-- > skipTo (TagOpen "div" [("class", "main")])
--
--   matches the first div opening tag that contains at least the attribute class with value main.
skipTo :: Tag T.Text -> ScraperAction ()
skipTo tag = skipWhile (~/= tag)
                `catchError` (\_ -> throwError "skipTo: could not find tag")

skipToText :: T.Text -> ScraperAction ()
skipToText txt = skipTo (TagText txt)
-- | Tries a computation, if it fails resets the state (tags), no errors are popped
try :: ScraperAction a -> ScraperAction (Maybe a)
try scrape = do
    state <- get
    let f' error = _runScraperAction (do put state >> return Nothing) -- Reset old state, it failed
        m'       = _runScraperAction (Just <$> scrape)
    ScraperAction $ catchError m' f'



--
-- ** Extra utilities
--


-- | Runs a scraper at a specific url, also renews its tags
atUrl :: T.Text -> ScraperAction a -> ScraperAction a
atUrl url scrape =
    do -- Save state
       oldState <- get

       -- Run a scraper with a different config
       absoluteUrl <- toAbsoluteUrl url
       tags <- liftIO $ downloadTags absoluteUrl
       put ([], [], tags)
       x <- local  (\config -> config {_url = T.unpack absoluteUrl, _tags = tags}) scrape

       -- Restore state
       put oldState

       return x

atUrls :: [T.Text] -> ScraperAction a -> ScraperAction [a]
atUrls urls scrape = mapM (flip atUrl scrape) urls

-- | Run a scrape and reset the stream of tags to the tags of the complete page
useAllTags :: ScraperAction a -> ScraperAction a
useAllTags scrape =
    do oldState <- get
       tags <- _tags <$> ask

       put ([], [], tags)
       x <- scrape
       put oldState
       return x

-- | Saves an image at the given url and returns the file path where it's saved
saveImage :: Text -> ScraperAction Text
saveImage url =
    do absoluteUrl <- toAbsoluteUrl url
       liftIO $ T.pack <$> downloadThumb absoluteUrl

-- | Splits on comma or the word "en"
splitEnumeration :: Text -> ScraperAction [Text]
splitEnumeration t = return (splitEnumeration' t)

splitEnumeration' :: Text -> [Text]
splitEnumeration' t = let splitted = concatMap (T.splitOn "/") (concatMap (T.splitOn " en ") (T.splitOn ", " t))
                      in  map (T.stripStart . T.stripEnd) splitted

-- | Attach an extra name to the scraper
(<?>) :: ScraperAction a -> String -> ScraperAction a
scraper <?> name = local (\config -> let name' = _name config ++ "." ++ name
                                     in config {_name = name'}) scraper

-- | Converts a given url to an absolute url (works as the identity on absolute urls,
--   uses the environment if it is relative to determine domain).
toAbsoluteUrl :: Text -> ScraperAction Text
toAbsoluteUrl t | isAbsoluteURI (T.unpack t)       = return t
                | isRelativeReference (T.unpack t) =
                      do let relative = fromJust . parseRelativeReference . T.unpack $ t
                         absolute    <- fromJust . parseURI . _url <$> ask
                         let combined = relative `relativeTo` absolute
                         return . T.pack . show $ combined
                | otherwise = throwError (T.unpack $ "toAbsoluteUrl: " <> t <> "is not a valid relative or absolute url")

simpleScrapeEvent :: ScraperAction Text                              -- ^ Title
                  -> ScraperAction [((Int, Int, Int), [(Int, Int)])] -- ^ Date Times
                  -> ScraperAction Text                              -- ^ Image path
                  -> ScraperAction Location                          -- ^ Location
                  -> ScraperAction [Text]                            -- ^ Performers
                  -> ScraperAction [(Int, Int)]                      -- ^ Price
                  -> ScraperAction Text                              -- ^ Description
                  -> ScraperAction Event
simpleScrapeEvent scrapeTitle scrapeDateTimes scrapeImage
                  scrapeLocation scrapePerformers scrapePrices
                  scrapeDescription =
  do __url        <- _url <$> ask
     _mimagePath  <- useAllTags (try scrapeImage      )
     _title       <- useAllTags (scrapeTitle          )
     _dateTimes   <- useAllTags (scrapeDateTimes      )
     _prices      <- useAllTags (scrapePrices         )
     _mperformers <- useAllTags (try scrapePerformers )
     _mdescr      <- useAllTags (try scrapeDescription)
     _location    <- useAllTags (scrapeLocation)
     return Event {
                       entryId     = EventId (-1),
                       url         = Url (T.pack __url),
                       title       = Title _title,
                       location    = _location,
                       dateTimes   = DateTimes _dateTimes,

                       performers  = Performers _mperformers,
                       program     = Program Nothing,
                       price       = Price (listToMaybe _prices), -- FIXME
                       description = Description _mdescr,
                       genre       = Genre Nothing,
                       image       = Image (fmap T.unpack _mimagePath)
                  }

simpleScrapeAll :: ScraperAction [Text]
                -> ScraperAction Event
                -> ScraperAction [Event]
simpleScrapeAll scrapeUrls scrapeEvent = do
    urls <- scrapeUrls
    let amount = length urls
    liftIO $ putStrLn ("Found " ++ show amount ++ " event urls")
    coupledLoop urls 0 amount
    --mbs <- mapM (\url -> try $ atUrl url scrapeInfo) urls
    --return (catMaybes mbs)
    where coupledLoop []         n amount = return []
          coupledLoop (url:urls) n amount = do liftIO $ putStrLn ("[" ++ show n ++ "/" ++ show amount ++ "]")
                                               liftIO $ putStrLn (T.unpack url)
                                               mb <- try (atUrl url scrapeEvent)
                                               case mb of
                                                   Nothing    -> do liftIO $ putStrLn "failed"
                                                                    coupledLoop urls (n+1) amount
                                                   Just entry -> do liftIO $ putStrLn "success"
                                                                    liftIO (print entry)
                                                                    entries <- coupledLoop urls (n+1) amount
                                                                    return (entry : entries)

