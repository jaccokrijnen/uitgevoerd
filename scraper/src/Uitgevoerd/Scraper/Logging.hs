-- | This module defines the datatype for messages and a way of printing
--   the message to the terminal

module Uitgevoerd.Scraper.Logging (Message (..),
                                   MessageType (..) ,
                                   writeMessageToFile) where
import System.Console.ANSI
import System.IO
import Uitgevoerd.Scraper.Utils
import Text.HTML.TagSoup
import qualified Data.Text as T

-- | The messages that can be output by the 'ScraperMonad'
data Message = Message { _messageType :: MessageType
                       , _url'        :: String
                       , _sender      :: String
                       , _tags'        :: Tags
                       , _message       :: String
                       }

-- | Encodes the different message types
data MessageType = MessageNormal
                 | MessageWarning
                 | MessageFail
                 | MessageHtml

instance Show Message where
    show msg  = unlines [ "url: "           ++ _url' msg
                        , "sender: "        ++ _sender msg
                        , "message: "       ++ _message msg
                        , "leftover tags: "
                        , T.unpack (renderTags (_tags' msg))]


writeMessageToFile :: Message -> FilePath -> IO ()
writeMessageToFile msg file =
    do writeFile file (show msg)

-- | Sets the foreground color
setColor :: Color -> IO ()
setColor c = setSGR [SetColor Foreground Vivid c]

-- | Resets the colors
resetColors :: IO ()
resetColors = setSGR [Reset] >> hFlush stdout





