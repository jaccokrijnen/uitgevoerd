module Uitgevoerd.Scraper (Scraper (..),
                           runScraper,
                           module Uitgevoerd.Scraper.ScraperAction,
                           module Uitgevoerd.Scraper.SmartParsers,
                           module Uitgevoerd.Scraper.Utils,
                           module Uitgevoerd.Scraper.Logging) where

import Uitgevoerd.Scraper.ScraperAction
import Uitgevoerd.Scraper.SmartParsers
import Uitgevoerd.Scraper.Utils
import Uitgevoerd.Scraper.Logging

import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction

import Control.Applicative

import Data.Time
import Data.Acid
import qualified Data.Text as T
import Data.Text (Text)
import Text.HTML.TagSoup

import System.FilePath
import System.Directory

-- | A 'Scraper' has a name, an initial url and a computation to extract
--   a list of entries.
data Scraper  = Scraper
    {
      name            :: String
    , startUrl        :: String
    , scrapeEntries   :: ScraperAction [Event]
    }

-- | Runs a scraper, logs the errors to disk and stores its results in the given database.
runScraper :: AcidState Database -> Scraper -> IO ()
runScraper db (Scraper name startUrl scrape) =
    do
       storeCookiesFrom startUrl
       tags <- parseTags <$> download startUrl

       let config = ScraperConfig name startUrl tags db
       (result, messages) <- runScraperAction config scrape

       let resultList = case result of
                   Left _  -> []
                   Right x -> x

       -- logging to terminal
       mapM_ print messages

       -- logging to file
       time <- getZonedTime
       let dirPath = "logs" </> show time
       createDirectoryIfMissing True dirPath
       mapM_ (\(msg, n) -> writeFile (dirPath </> show n <.> "log") (show msg)) (zip messages ([0..] :: [Int]))

       -- inserting entries
       mapM_ (\entry -> update db (InsertEvent entry)) resultList

       return ()