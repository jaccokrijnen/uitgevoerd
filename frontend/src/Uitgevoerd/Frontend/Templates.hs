{-# LANGUAGE FlexibleContexts
           , RecordWildCards
           , OverlappingInstances
           , QuasiQuotes
           , OverloadedStrings
           , FlexibleInstances
           , TypeSynonymInstances
           , GADTs #-}

module Uitgevoerd.Frontend.Templates where

import Control.Applicative        ((<$>))
import Control.Monad.Identity     (Identity(runIdentity))
import Control.Monad.State
import Data.String                (IsString(fromString))
import qualified Data.Text as T
import Data.Text (Text)
import Data.Text.Lazy             (fromChunks)
import Data.Monoid
import Data.Maybe
import Data.List
import Data.Function

import Language.Haskell.HSX.QQ    (hsx)         -- The preprocessor for embedded XML
import HSP                                      -- The types that are used by the preprocessor
import HSP.Monad                  (HSPT(..))
import HSP.HTML4

import Language.Javascript.JMacro
import HSP.JMacro
import Happstack.Server.JMacro


import qualified Happstack.Server.HSP.HTML as HappHTML
import Happstack.Server
import Happstack.Server.XMLGenT

import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Unwrap

------------------------------------------------------------------------------------
-- Note to self: HSP (xml) and hsx2hs (quasiquoter for HSP) are awfully general and extensible...
-- In this code I only want to generate pure xml (html) fragments, using
-- the nice quasiquoter syntax. The simplification (being less general) means:
--     * No need to use monad transformers
--     * Only use a single xml representation: the XML type from the HSP library
------------------------------------------------------------------------------------


------------------------------------------------
-- Wrappers (simplifying the hsx2hs and HSP interface)
------------------------------------------------


-- | The (specialized) Html generator
--   Monad stack:
--      * HSPT  XML: identity monad that has a phantom type (this case: XML) allowing
--                   multiple instances of XMLGenerator
--      * State Int: Contains fresh integer supply for javscript generation
--
--   note: The type synonym does not contain the type parameter (this makes it possible to use it
--         as kind * -> * in constrains for example)
type HtmlBuilder = HSPT XML (State Integer)


instance IntegerSupply HtmlBuilder where
    nextInteger = lift (do x <- get
                           put (x + 1)
                           return x)


-- | Runs the monad stack, builds generated XML from the generator
runHtmlBuilder :: HtmlBuilder XML -> XML
runHtmlBuilder = flip evalState 0 . unHSPT




-- | Specialized default template that returns XML
defaultTemplate :: (EmbedAsChild HtmlBuilder headers,
                    EmbedAsChild HtmlBuilder body)
                => Text
                -> headers
                -> body
                -> HtmlBuilder XML
defaultTemplate t h b = HappHTML.defaultTemplate lazyText h b
    where lazyText = fromChunks [t]





-------------------------------------------------
-- Templates
-------------------------------------------------


-- | Renders a page containing a list of entries
renderMatches :: [Event]      -- All the search results
              -> Response     -- Http response with the body
renderMatches entries = let xml = runHtmlBuilder $ do headerChilds <- header
                                                      bodyXml   <- makeBody entries

                                                      defaultTemplate "Resultaten" headerChilds bodyXml

                        in  toResponse xml


-- | Refers to the css files, UTF-8 and settings for bootstrap
header :: HtmlBuilder [ChildType HtmlBuilder]
header = unXMLGenT $ [hsx|
         <%>
             <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
             <link href="css/bootstrap.css" rel="stylesheet" media="screen" />
             <link href="css/bootstrap.min.css" rel="stylesheet" />
             <link href="css/custom.css" rel="stylesheet" />

             <meta charset="utf-8" />
             <meta http-equiv="X-UA-Compatible" content="IE=edge" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
         </%>
         |]
  where js = [jmacro|
            fun toggleDescription resultDataDiv
            {
                var elemShort = resultDataDiv.getElementsByClassName('result-descr-short')[0];
                var elemLong  = resultDataDiv.getElementsByClassName('result-descr-long')[0];

                if (elemShort.style.display == 'block')
                {
                    elemShort.style.display = 'none';
                    elemLong.style.display = 'block';
                }
                else
                {
                    elemShort.style.display = 'block';
                    elemLong.style.display = 'none';
                }
            };

            window.onload = function() {
              entries = document.getElementsByClassName('row result-item');
              console.log(entries.length);
              for (var i = 0; i < entries.length; i++)
              {
                  fun f entry {
                    var a     = entry.getElementsByClassName("result-readmore")[0];
                    a.onclick = function() { toggleDescription(entry) };
                  };
                  f(entries[i]);
              }
            }


           |]


-- | The actual page
makeBody :: [Event]     -- All the search results
         -> HtmlBuilder XML         -- Body html
makeBody entries =
  do let groupedEntries = groupBy ((==) `on` firstDate) entries
     entriesChilds <- mapM viewGroupedEntries groupedEntries
     let childs = concat entriesChilds
     venueLinks <- mapM linkToVenue allVenues
     unXMLGenT $ [hsx|
         <div class="container">

             <div class="row">
                 <div class="col-xs-10 col-xs-offset-1 result-title">
                     <h1 class="">Resultaten</h1>
                 </div>
             </div>

             <div class="row search-thing">


                 <div class="col-xs-4 search-left">


                     <form class="navbar-form" role="search">

                         <h3>Zoeken</h3>

                         <div class="input-group">
                             <span class="input-group-addon">Locatie</span>
                             <input type="text" class="form-control" placeholder="Stad, gebouw" name="loc" id="loc" />
                         </div>


                         <br />

                         <div class="input-group">
                             <span class="input-group-addon">Detail</span>
                             <input type="text" class="form-control" placeholder="Componist, werk, uitvoerenden" name="q" id="q" />
                         </div>

                         <br />
                         <div class="input-group">
                         <button class="btn btn-default pull-right" type="submit">Zoek</button>
                         </div>

                         <% concat venueLinks %>

                     </form>
                 </div>

                 <div class="col-xs-8 results nopadding">
                     <% childs %>
                 </div>

             </div>
         </div>
         |]

firstDate :: Event -> (Int, Int, Int)
firstDate = fst . head . unwrap . dateTimes


allVenues = ["Het Concertgebouw", "Frits Filips Muziekgebouw", "Muziekgebouw aan 't IJ", "Rotterdamse Schouwburg", "Stadsschouwburg Amsterdam", "Stadsschouwburg Groningen De Oosterpoort", "Stadsschouwburg Utrecht"]

toQueryStr = T.intercalate "+" . T.split ( == ' ')

linkToVenue :: Text -> HtmlBuilder [ChildType HtmlBuilder]
linkToVenue name = unXMLGenT [hsx|
<%>
    <a href=url> <% name %> </a><br />
</%>|]
    where url = "result?venue=" <> toQueryStr name


viewGroupedEntries :: [Event] -> HtmlBuilder [ChildType HtmlBuilder]
viewGroupedEntries events =
    do childrenEvents <- mapM viewDebugEntry events
       dateChild      <- dateDiv
       return (dateChild ++ concat childrenEvents)

    where dateDiv = unXMLGenT [hsx|
<%>
    <div class="row date-item">
        <h4><% formatDate date %></h4>
    </div>
</%>
|]
          date = firstDate (head events)

viewEntry :: Event -> HtmlBuilder [ChildType HtmlBuilder]
viewEntry Event {..} = unXMLGenT [hsx|
<%>
    <div class="row result-item">
        <div class="result-img col-xs-2">
            <img src=formatImage />
        </div>

        <div class="result-data col-xs-10">

              <h4><a href="#"> <% unwrap title  %> </a></h4>

              <div class="result-descr-short"> <% formatDescrShort %> </div>
              <span class="pull-left"><a href="#" class="result-readmore"> lees meer.. </a></span>

              <span class="pull-right result-details">
                  <% formatDateTimes (unwrap dateTimes) %>
                  |
                  <% formatLocation %>
                  |
                  <% formatPrice %>
              </span>
        </div>
    </div>

    <hr />
</%>|]

   where formatImage     = maybe "img/image_unavailable.png" T.pack (unwrap image)

         -- id's for the div that contains the short and long descriptions
         descrIdShort = "descrIdShort" <> T.pack (show (unwrap entryId))
         descrIdLong  = "descrIdLong"  <> T.pack (show (unwrap entryId))

         {- formatOnclick   = case unwrap url of
                               Just url -> "window.open('" <> url <> "');" :: Text
                               Nothing  -> "" :: Text -}
         formatDescrShort = case unwrap description of
                               Just descr -> if T.length descr > 400 then T.take 200 descr <> "..." else descr
                               Nothing    -> "Geen beschrijving beschikbaar."
         formatLocation   = unwrap (_venue location) <> ", " <> unwrap (_town location)
         formatDescrLong  = "Long description..." :: T.Text
         formatPrice     = case unwrap price of
                               Just (eur, cents) -> "vanaf €"
                                                 <> showT eur
                                                 <> "," <> if cents == 0
                                                               then "-"
                                                               else showZeroPrefix cents
                               Nothing           -> "prijs onbekend"


-- | For testing purposes: shows all the info of the event
viewDebugEntry :: Event -> HtmlBuilder [ChildType HtmlBuilder]
viewDebugEntry Event {..} = unXMLGenT [hsx|
<%>
    <div class="row result-item">
        <div class="result-img col-xs-2">
            <img src=formatImage />
        </div>

        <div class="result-data col-xs-10">

              <b>Titel: </b> <span> <% unwrap title%></span> <br />
              <b>Beschrijving: </b><span><% formatDescrShort%></span><br />
              <b>Locatie: </b><span><% formatLocation %></span><br />
              <b>Data/Tijden: </b><span><% formatDateTimes (unwrap dateTimes)%></span><br />
              <b>Uitvoerenden: </b><span><% formatPerformers %></span><br />
              <b>Programma: </b><span><% formatProgram%></span><br />
              <b>Genre: </b><span><% formatGenre%></span><br />
              <b>Link: </b><span><a href=(unwrap url)> <% unwrap url %></a></span><br />
              <b>Prijs: </b><span><% formatPrice%></span><br />
              <b>Image: </b><span><% formatImage %></span> <br/>
        </div>
    </div>

    <hr />
</%>|]

   where formatImage     = maybe "img/image_unavailable.png" T.pack (unwrap image)
         formatPerformers = maybe "N/A" (T.concat . intersperse ", ") (unwrap performers)
         formatProgram =  "N/A" :: Text
         formatGenre = "N/A" :: Text


         -- id's for the div that contains the short and long descriptions
         descrIdShort = "descrIdShort" <> T.pack (show (unwrap entryId))
         descrIdLong  = "descrIdLong"  <> T.pack (show (unwrap entryId))

         {- formatOnclick   = case unwrap url of
                               Just url -> "window.open('" <> url <> "');" :: Text
                               Nothing  -> "" :: Text -}
         formatDescrShort = case unwrap description of
                               Just descr -> if T.length descr > 400 then T.take 200 descr <> "..." else descr
                               Nothing    -> "N/A"
         formatLocation   = unwrap (_venue location) <> ", " <> unwrap (_town location)
         formatDescrLong  = "Long description..." :: T.Text
         formatPrice     = case unwrap price of
                               Just (eur, cents) -> "vanaf €"
                                                 <> showT eur
                                                 <> "," <> if cents == 0
                                                               then "-"
                                                               else showZeroPrefix cents
                               Nothing           -> "prijs onbekend"

---------------------------------------------------
-- Formatting utilities
---------------------------------------------------

-- | Shows a value as Text
showT :: (Show a) => a -> Text
showT = T.pack . show


-- | Shows an int as Text. Adds a zero prefix when it is in [0..9]
showZeroPrefix :: Int -> Text
showZeroPrefix n
            | n < 10 && n >= 0 = "0" <> showT n
            | otherwise        = showT n


-- | Formats a [(Date, Times)] where type Date = (Int, Int, Int) and type Times = [(Int, Int)]
-- Renders like:
--   17/07/1992  10:30
--               12:00
--               14:00
--   1/2/2014
--   31/4/1592   11:00
formatDateTimes :: [(Date, Times)] -> Text
formatDateTimes dateTimes =
    T.intercalate "\n" $
       do (date, times) <- dateTimes
          let fdate     =  formatDate date
          let ftimes    =  map formatTime times
          case ftimes of
              []   -> return $ fdate
              t:ts -> let firstLine = fdate <> " " <> t
                          others    = map (T.justifyRight (T.length firstLine) ' ') ts
                      in  firstLine : others

formatDate :: Date -> Text
formatDate (y, m, d) = showZeroPrefix d <> "/"
                            <> showZeroPrefix m <> "/"
                            <> showZeroPrefix y

formatTime :: (Int, Int) -> Text
formatTime (h, m)    = showZeroPrefix h <> ":" <> showZeroPrefix m
