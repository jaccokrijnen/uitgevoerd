{-# LANGUAGE  OverloadedStrings
            , QuasiQuotes
            , GeneralizedNewtypeDeriving
            , RecordWildCards #-}

module Uitgevoerd.Frontend.Main where

import Happstack.Server
import Happstack.Server.FileServe
import Happstack.Server.Response

import           Text.Blaze ((!))
import qualified Text.Blaze.Html4.Strict as H
import qualified Text.Blaze.Html4.Strict.Attributes as A

import Paths_frontend
import Control.Monad
import Control.Applicative
import Control.Exception
import Control.Monad.Trans (liftIO)
import System.FilePath

import Data.Acid
import Data.Acid.Advanced
import Data.Acid.Local
import Data.Char
import Data.SafeCopy
import qualified Data.ByteString.Lazy.Char8 as B
import Data.Monoid
import qualified Data.Text as T

import Uitgevoerd.Database.Types
import Uitgevoerd.Database.Interaction
import Uitgevoerd.Frontend.Templates

import Data.Default

main :: IO ()
main = do putStrLn "Opening database ..."
          bracket (openLocalState def)

                  (createCheckpointAndClose)

                  (\acid -> do putStrLn "Succesfully opened database!"
                               simpleHTTP nullConf (routing acid))

          main    -- in case the connection with the database fails



routing :: AcidState Database -> ServerPartT IO Response
routing acid = msum [ dir "result" $ queryVenue acid
                    , dir "css"    $ css
                    , dir "js"     $ js
                    , dir "img"    $ img
                    , dir "fonts"  $ fonts
                    , index]


------------------------------
-- Serverparts
-------------------------------


index :: ServerPart Response
index = do
    path <- liftIO $ getDataFileName "files/index.html"
    serveDirectory EnableBrowsing ["index.html"] path


css :: ServerPart Response
css = serveDirectory DisableBrowsing [] "files/css"


js :: ServerPart Response
js = serveDirectory DisableBrowsing [] "files/js"


img :: ServerPart Response
img = serveDirectory DisableBrowsing [] "files/img"

fonts :: ServerPart Response
fonts = serveDirectory DisableBrowsing [] "files/fonts"

-- Note: De State directory met de database erin is nu gewoon gekopieerd uit het scraper project
-- (bevat vredenburg)
queryPart :: AcidState Database -> ServerPart Response
queryPart acid = do
                    q   <- look "q"
                    loc <- look "loc"
                    let keywords      = map (T.map toLower) . T.words . T.pack $ q
                        locationWords = map (T.map toLower) . T.words . T.pack $ loc
                    liftIO (print keywords)
                    liftIO (print locationWords)
                    matches  <- query' acid (QQuery keywords locationWords)

                    -- This putStrLn is to show that strings are correctly
                    -- encoded in the bytestring type, not when converted to string :/
                    -- liftIO $ B.putStrLn (B.concat $ map ((\(Title bstr) -> bstr) . title) matches)


                    -- ok $ toResponseBS "text/html; charset=utf8"
                    --                   (B.unlines ("Results: " : map ((\(Title bstr) -> bstr) . title) matches))
                    ok $ renderMatches matches

queryVenue :: AcidState Database -> ServerPart Response
queryVenue acid = do
                    venue   <- lookText' "venue"
                    let keywords      = T.toLower venue
                    liftIO (putStrLn "Searching for a venue")
                    matches  <- query' acid (QVenue venue)

                    -- This putStrLn is to show that strings are correctly
                    -- encoded in the bytestring type, not when converted to string :/
                    -- liftIO $ B.putStrLn (B.concat $ map ((\(Title bstr) -> bstr) . title) matches)


                    -- ok $ toResponseBS "text/html; charset=utf8"
                    --                   (B.unlines ("Results: " : map ((\(Title bstr) -> bstr) . title) matches))
                    ok $ renderMatches matches

{-
appTemplate :: String -> [H.Html] -> H.Html -> H.Html
appTemplate title headers body =
    H.html $ do
      H.head $ do
        H.title (H.toHtml title)
        H.meta ! A.httpEquiv "Content-Type"
               ! A.content "text/html;charset=utf-8"
        sequence_ headers
      H.body $ do
        body

helloBlaze :: ServerPart Response
helloBlaze =
   ok $ toResponse $
    appTemplate "Hello, Blaze!"
                [H.meta ! A.name "keywords"
                        ! A.content "happstack, blaze, html"
                ]
                (H.input ! A.name "q"
                         ! A.type_ "text"

                (H.p $ do "Hello, "
                          H.b "blaze-html!") -}
